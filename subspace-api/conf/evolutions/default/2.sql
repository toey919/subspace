# --- !Ups

CREATE TABLE `Issues` (
  `id` CHAR(36) NOT NULL PRIMARY KEY,
  `owner_id` CHAR(48) NOT NULL,
  `repository_id` CHAR(36) NOT NULL,
  `title` TEXT NOT NULL,
  `description` TEXT DEFAULT NULL,
  `priority` FLOAT NOT NULL DEFAULT 0, 
  `priority_up_vote_points` FLOAT NOT NULL DEFAULT 0,
  `priority_down_vote_points` FLOAT NOT NULL DEFAULT 0,
  `priority_all_vote_points` FLOAT NOT NULL DEFAULT 0,
  `is_closed` BOOLEAN NOT NULL DEFAULT 0,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (repository_id) REFERENCES Repositories(id),
  FOREIGN KEY (owner_id) REFERENCES Users(id)
);

CREATE TABLE `IssuePriorityVotes` (
  `owner_id` CHAR(48) NOT NULL,
  `issue_id` CHAR(36) NOT NULL,
  `vote_point` FLOAT NOT NULL,
  `is_vote_up` BOOLEAN NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (issue_id) REFERENCES Issues(id),
  FOREIGN KEY (owner_id) REFERENCES Users(id),
  PRIMARY KEY (owner_id, issue_id)
);


CREATE TABLE `IssueComments` (
  `id` CHAR(36) NOT NULL PRIMARY KEY,
  `owner_id` CHAR(48) NOT NULL,
  `issue_id` CHAR(36) NOT NULL,
  `content` TEXT NOT NULL,
  `parent_id` CHAR(36),
  `total_up_vote_points` float NOT NULL DEFAULT '0',
  `total_down_vote_points` float NOT NULL DEFAULT '0',
  `total_all_vote_points` float NOT NULL DEFAULT '0',
  `up_vote_count` int(11) NOT NULL DEFAULT '0',
  `down_vote_count` int(11) NOT NULL DEFAULT '0',
  `all_vote_count` int(11) NOT NULL DEFAULT '0',
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (issue_id) REFERENCES Issues(id),
  FOREIGN KEY (owner_id) REFERENCES Users(id),
  FOREIGN KEY (parent_id) REFERENCES IssueComments(id)
);

CREATE TABLE `IssueCommentVotes` (
  `owner_id` CHAR(48) NOT NULL,
  `issue_comment_id` CHAR(36) NOT NULL,
  `vote_point` FLOAT NOT NULL,
  `is_vote_up` BOOLEAN NOT NULL,
  `created_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  FOREIGN KEY (issue_comment_id) REFERENCES IssueComments(id),
  FOREIGN KEY (owner_id) REFERENCES Users(id),
  PRIMARY KEY (owner_id, issue_comment_id)
);


# --- !Downs

DROP TABLE if exists `IssuePriorityVotes`;
DROP TABLE if exists `IssueCommentVotes`;
DROP TABLE if exists `IssueComments`;
DROP TABLE if exists `Issues`;
