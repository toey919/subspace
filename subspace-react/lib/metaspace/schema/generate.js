const http = require('http');
const path = require('path');
const fs = require('fs');

const options = {
  host: 'localhost',
  port: 9000,
  path: '/render-schema',
};


const request = new Promise((resolve) => {
  const callback = (response) => {
    let str = '';
    response.on('data', (chunk) => { str += chunk; });
    response.on('end', () => resolve(JSON.parse(str)));
  };
  http.request(options, callback).end();
});

const writeSchema = schema => new Promise((resolve, reject) => {
  const filename = path.resolve(__dirname, 'schema.json');
  const data = JSON.stringify(schema, null, '  ');
  fs.writeFile(filename, data, err => (err ? reject(err) : resolve('Successfully generated schema.json')));
});

// eslint-disable-next-line
request.then(writeSchema).then(console.log, console.log);
