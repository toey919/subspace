import React from 'react';
import PropTypes from 'prop-types';
import { find, propEq } from 'ramda';

class CodeElement extends React.Component {
  constructor(props) {
    super(props);
    this.state = { expanded: false };
  }

  render() {
    const { name, type, children } = find(propEq('uid', this.props.uid), this.props.elements);
    const cursor = children.length ? 'pointer' : 'default';
    return (
      <div className="pl3">
        <div
          role="button"
          tabIndex="0"
          style={{ cursor }}
          onClick={() => this.setState({ expanded: !this.state.expanded })}
        >
          <span>{this.props.global ? 'global' : type}</span>
          <span>{name ? ` ${name}` : ''}</span>
          {!!children.length &&
            <span>{this.state.expanded ? ' -' : ' +'}</span>
          }
        </div>
        {this.state.expanded && children.map(({ uid }) => (
          <CodeElement
            uid={uid}
            elements={this.props.elements}
          />
        ))}
      </div>
    );
  }
}

CodeElement.defaultProps = {
  global: false,
};

CodeElement.propTypes = {
  uid: PropTypes.number.isRequired,
  global: PropTypes.boolean,
  elements: PropTypes.arrayOf(PropTypes.shape({
    name: PropTypes.string,
    type: PropTypes.string.isRequired,
    uid: PropTypes.number.isRequired,
  })).isRequired,
};

export default CodeElement;
