import React from 'react';
import PropTypes from 'prop-types';
import { QueryRenderer, graphql } from 'react-relay';

import environment from '../../relay/environment';

import CodeElement from './CodeElement';

const CodePreview = (props) => {
  const elements = props.elements.map(element => (
    element.type === 'call' ? { ...element, name: element.name.split('|')[1] } : element
  ));
  return (
    <div>
      <h3>
        Code Preview
      </h3>
      <div style={{ whiteSpace: 'pre-wrap' }} className="f4">
        <CodeElement
          uid={props.root.uid}
          depth={0}
          elements={elements}
          global
        />
      </div>
    </div>
  );
};

CodePreview.propTypes = {
  elements: PropTypes.arrayOf(PropTypes.shape({
    uid: PropTypes.number.isRequired,
    name: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    children: PropTypes.arrayOf(PropTypes.shape({
      uid: PropTypes.number.isRequired,
    })).isRequired,
  })).isRequired,
  root: PropTypes.shape({
    uid: PropTypes.string.isRequired,
  }).isRequired,
};

export default ownProps => (
  <QueryRenderer
    environment={environment}
    query={graphql`
      query CodePreviewQuery($code: String!) {
        root: ast(code: $code) {
          uid
        }
        elements: statements(code: $code) {
          uid
          name
          type
          children {
            uid
          }
        }
      }
    `}
    variables={{
      code: ownProps.code,
    }}
    render={({ error, props }) => {
      if (error) {
        return <div>{error.message}</div>;
      } else if (props) {
        return <CodePreview {...props} />;
      }
      return <div>Loading</div>;
    }}
  />
);

