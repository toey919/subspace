import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

const Tab = props => (
  // eslint-disable-next-line
  <div
    className={cn('pv3 pr3', { underline: !props.selected, pointer: !props.selected })}
    onClick={() => props.onClick(props.id)}
  >
    {props.label}
  </div>
);

Tab.propTypes = {
  id: PropTypes.any.isRequired,
  label: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  selected: PropTypes.bool.isRequired,
};

export default Tab;
