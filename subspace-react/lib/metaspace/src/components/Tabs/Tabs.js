import React from 'react';
import PropTypes from 'prop-types';
import { range, zipWith } from 'ramda';

import Tab from './Tab';

const Tabs = (props) => {
  const ids = props.ids.length === 0 ? range(0, props.labels.length) : props.ids;
  return (
    <div className="flex">
      {zipWith((id, label) => ({ id, label }), ids, props.labels).map(({ id, label }) => (
        <Tab
          id={id}
          label={label}
          selected={props.selected === id}
          onClick={props.onClick}
        />
      ))}
    </div>
  );
};

Tabs.defaultProps = {
  ids: [],
};

Tabs.propTypes = {
  labels: PropTypes.arrayOf(PropTypes.string).isRequired,
  ids: PropTypes.array,
  // eslint-disable-next-line
  selected: PropTypes.any.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default Tabs;
