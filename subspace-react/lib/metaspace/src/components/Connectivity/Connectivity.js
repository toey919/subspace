/* eslint-disable no-param-reassign */
/* eslint-disable no-return-assign */
/* eslint-disable no-multi-assign */
/* eslint-disable class-methods-use-this */

import React from 'react';
import PropTypes from 'prop-types';
import { createFragmentContainer, graphql } from 'react-relay';
import { prop, map, pipe, groupBy, head, sort, zip, dissoc, values, path, find, propEq, pathEq, filter, identity, reduce, keys, max, clone, uniq } from 'ramda';
import { Observable } from 'rxjs';
import * as d3 from 'd3';

import css from './Connectivity.scss';

class Connectivity extends React.Component {
  componentDidMount() {
    this.svg = d3.select(this.container).append('svg');
    this.svg
      .append('g')
      .attr('id', 'container');
    this.nodesContainer = this.svg
      .select('#container')
      .append('g')
      .attr('id', 'nodes');
    this.linksContainer = this.svg
      .select('#container')
      .append('g')
      .attr('id', 'links');
    this.renderLayout();
    this.resizeSubscription =
      Observable.fromEvent(window, 'resize')
        .debounceTime(500)
        .subscribe(this.renderLayout);
  }

  componentWillUnmount() {
    this.resizeSubscription.unsubscribe();
  }

  /**
   * Builds a d3 hierarchy from graph. While in reality we can have many parents,
   * here we reduce many parents to one, using resolveParent function.
   * 
   * The other trick is we have to clone each non-leaf node so that it will be leaf.
   * This is needed in order to make all classes appear as nodes on one level, not
   * intersecting.
   */
  getHierarchy() {
    // Create a hashmap of classes for quick access by uid
    const classes = pipe(
      map(prop('vertex')),
      map(vertex => ({ ...vertex, parents: [] })),
      groupBy(prop('uid')),
      map(head),
    )(this.props.hierarchy.layeredVertices);

    // fill parents
    this.props.hierarchy.edges.forEach(({ from, to }) => {
      classes[to.uid].parents.push(classes[from.uid]);
    });
    const classesList = values(classes);
    // Global parent is a requirement of d3, we need it since we have many roots
    const globalParent = { uid: 0, name: 'global', type: 'global' };
    // transform many parents to one, add global parent where necessary
    const stratifiedClasses = pipe(
      map(this.resolveParents),
      zip(classesList),
      map(classAndParent => ({ ...classAndParent[0], parent: classAndParent[1] })),
      map(dissoc('parents')),
      map(cls => (cls.parent.uid ? cls : { ...cls, parent: globalParent })),
    )(classesList);

    // each clone needs an id - this is id generator
    const maxUid = pipe(
      keys,
      map(x => parseInt(x, 10)),
      reduce(max)(0),
    )(classes);
    const newUid = (() => {
      let current = maxUid + 1;
      // eslint-disable-next-line no-plusplus
      return () => current++;
    })();

    const branchClones = pipe(
      map(path(['parent', 'uid'])),
      filter(identity),
      uniq,
      map(uid => find(propEq('uid', uid))(stratifiedClasses)),
      map((cls) => {
        const cloned = pipe(clone, dissoc('parent'))(cls);
        const stratifiedCls = find(propEq('uid', cls.uid))(stratifiedClasses);
        // note that we need old uid to be in a leaf, so our class interconnections work
        // correctly. Non-leaf will have new id and used only for line drawing.
        stratifiedCls.uid = newUid();
        cloned.parent = stratifiedCls;
        // update connections to old node with new id
        stratifiedClasses.forEach((klass) => {
          if (klass.parent.uid === cloned.uid) klass.parent.uid = stratifiedCls.uid;
        });
        return cloned;
      }),
    )(stratifiedClasses);
    const allClasses = [...stratifiedClasses, ...branchClones, globalParent];
    const stratify = d3.stratify().id(prop('uid')).parentId(path(['parent', 'uid']));
    return stratify(allClasses);
  }

  mouseovered = (d) => {
    this.nodes.each(n => (n.target = n.source = false));

    this.links
      .classed(css['link--target'], l => (l.target === d ? (l.source.source = true) : null))
      .classed(css['link--source'], l => (l.source === d ? (l.target.target = true) : null))
      .filter(l => (l.target === d || l.source === d))
      .raise();

    this.nodes
      .classed(css['node--target'], prop('target'))
      .classed(css['node--source'], prop('source'));
  }

  mouseouted = () => {
    this.links
      .classed(css['link--target'], false)
      .classed(css['link--source'], false);

    this.nodes
      .classed(css['node--target'], false)
      .classed(css['node--source'], false);
  }


  /**
   * Though we can have multiple parents, to make visualization
   * we need to reduce multiple parents to one. This is done according
   * to priorities - obj has the highest priority, interface - the lowest.
   */
  resolveParents(cls) {
    const scores = {
      obj: 4,
      class: 3,
      trait: 2,
      interface: 1,
    };
    const comparator = (a, b) => -((scores[a.type] || 0) - (scores[b.type] || 0));
    return pipe(
      sort(comparator),
      head,
      dissoc('parents'),
    )(cls.parents);
  }

  renderLayout = () => {
    const { offsetWidth: width, offsetHeight: height } = this.container;
    const radius = Math.min(width, height) / 2;
    const innerRadius = radius * 0.7;

    this.svg
      .attr('width', radius * 2)
      .attr('height', radius * 2)
      .select('#container')
      .attr('transform', `translate(${radius}, ${radius})`);

    // Build d3 hierarchy based on class inheritance relationships
    const root = this.getHierarchy();

    // Build a cluster (a tree where all leaves are on the same level)
    // This function adds x, y coordinates to each node (x ranging in 0 - 360, y in 0 - innerRadius)
    // So that each node represent a leaf / branch of a cluster
    d3.cluster().size([360, innerRadius])(root);

    // We are using a trick in a hierarchy so that we duplicate each branch (node that has children)
    // so that this branch will also have a clone leaf. 
    // That way root.leaves() contains every class with different x - coordinate, ranging from
    // 0 to 360. Close classes in terms of hierarchy have close x coordinate
    const nodesData = filter(node => node.data.uid !== 0)(root.leaves());

    // Here we simply map our x 0 to 360 coordinate to angle on a circle
    if (this.nodes) this.nodes.remove();
    this.nodes = this.nodesContainer
      .selectAll(css.node)
      .data(nodesData)
      .enter()
      .append('text')
      .text(d => d.data.name)
      .attr('class', css.node)
      .attr('dy', '0.31em')
      .on('mouseover', this.mouseovered)
      .on('mouseout', this.mouseouted);

    this.nodes
      .attr('transform', d => `rotate(${d.x - 90})translate(${d.y}, 0)${d.x < 180 ? '' : 'rotate(180)'}`)
      .attr('text-anchor', d => (d.x < 180 ? 'start' : 'end'));

    // We can supply to line a set of objects with x, y coordinates
    // and it will draw the splince through this points. Note that we convert
    // x to polar coordinates as well
    const line = d3.lineRadial()
      .radius(prop('y'))
      .angle(d => (d.x / 180) * Math.PI)
      .curve(d3.curveBundle.beta(0.85));
    // Path function returns path in a hierachy from one node to another
    // That way we kind of draw the line through parents in a tree
    // (which remains invisible nodes, as we used root.leaves())
    const curvedPath = d => line(d.source.path(d.target));

    const linksData = map(edge => ({
      source: find(pathEq(['data', 'uid'], edge.from.uid))(nodesData),
      target: find(pathEq(['data', 'uid'], edge.to.uid))(nodesData) }),
    )(this.props.connectivity);

    if (this.links) this.links.remove();
    this.links = this.linksContainer
      .selectAll(css.link)
      .data(linksData)
      .enter()
      .append('path')
      .attr('class', css.link)
      .attr('d', curvedPath);
  }

  render() {
    return (
      <div className="flex-grow f3" ref={(container) => { this.container = container; }} />
    );
  }
}

Connectivity.propTypes = {
  connectivity: PropTypes.object.isRequired,
  hierarchy: PropTypes.object.isRequired,
};

export default createFragmentContainer(
  Connectivity,
  graphql`
    fragment Connectivity_connectivity on Edge @relay(plural: true) {
      from {
        uid
        name
        type
      }
      to {
        uid
        name
        type
      }
    }
    
    fragment Connectivity_hierarchy on LayeredGraph {
      layeredVertices {
        layer
        vertex {
          uid
          name
          type        
        }
      }
      edges {
        from {
          uid
        }
        to {
          uid
        }
      }
    }
  `,
);
