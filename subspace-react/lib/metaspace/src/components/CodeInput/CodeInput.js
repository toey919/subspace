import React from 'react';
import PropTypes from 'prop-types';

const CodeInput = props => (
  <div className="h-100 flex flex-column">
    <h3>Code Input</h3>
    <textarea
      value={props.value}
      onChange={e => props.onChange(e.target.value)}
      className="w-100 flex-grow"
    />
  </div>
);

CodeInput.propTypes = {
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default CodeInput;
