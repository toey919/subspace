/* eslint-disable global-require, import/prefer-default-export */
import App from './components/App';

if (__RENDER__) {
  require('./components');
}

export {
  App,
};
