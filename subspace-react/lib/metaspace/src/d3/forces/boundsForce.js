/* eslint-disable no-mixed-operators */
/* eslint-disable no-param-reassign */

/**
 * Pulls nodes towards a predefined layer
 */
export default () => {
  let strength;
  let width;
  let height;
  let nodes;
  let strengths;

  function force(alpha) {
    // factor to normalize force from 0 to 1
    const factor = 10000;
    nodes.forEach((node, i) => {
      const charLength = 9; // approximate for font size 18  
      const length = node.name.length * charLength;
      const x = Math.max(1, Math.min(width() - 1, node.x));
      const y = Math.max(1, Math.min(height() - 1, node.y));
      const xForce = (alpha * strengths[i]) / (x) -
        (alpha * strengths[i]) / (Math.max((width() - x - length), 1));
      const yForce = (alpha * strengths[i]) / (y) -
        (alpha * strengths[i]) / ((height() - y));
      node.vx += xForce * factor;
      node.vy += yForce * factor;
    });
  }

  function initialize() {
    if (!nodes) return;

    // populate local `strengths` using `strength` accessor
    // basically strenght can be a function of 3 variables - node, i, nodes
    // e.g. layerForce().strength((node, i, nodes) => ...)
    strengths = nodes.map((node, i) => strength(node, i, nodes));
  }

  force.initialize = (initialNodes) => {
    nodes = initialNodes;
    initialize();
  };

  // if called with zero args with get strength
  // if calls with args - will set strength
  force.strength = (_) => {
    // return existing value if no value passed
    if (_ == null) return strength;

    // coerce `strength` accessor into a function
    strength = typeof _ === 'function' ? _ : () => +_;

    // reinitialize
    initialize();

    // allow chaining
    return force;
  };

  force.width = (_) => {
    if (_ == null) return height;
    width = typeof _ === 'function' ? _ : () => _;
    initialize();
    return force;
  };

  force.height = (_) => {
    if (_ == null) return height;
    height = typeof _ === 'function' ? _ : () => +_;
    initialize();
    return force;
  };

  if (!strength) force.strength(0.1);
  if (!width) force.width(400);
  if (!height) force.height(400);

  return force;
};
