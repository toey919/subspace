'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _CodeInput = require('./CodeInput');

var _CodeInput2 = _interopRequireDefault(_CodeInput);

var _CodePreview = require('./CodePreview');

var _CodePreview2 = _interopRequireDefault(_CodePreview);

var _Visuals = require('./Visuals');

var _Visuals2 = _interopRequireDefault(_Visuals);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var App = function (_React$Component) {
  _inherits(App, _React$Component);

  function App(props) {
    _classCallCheck(this, App);

    var _this = _possibleConstructorReturn(this, (App.__proto__ || Object.getPrototypeOf(App)).call(this, props));

    _this.handleCodeUpdate = function (code) {
      _this.setState({ code: code });
      _this.localStorage.setItem('code', code);
    };

    _this.localStorage = window ? window.localStorage : { getItem: function getItem() {}, setItem: function setItem() {} };
    _this.state = { code: _this.localStorage.getItem('code') || '' };
    return _this;
  }

  _createClass(App, [{
    key: 'render',
    value: function render() {
      return _react2.default.createElement(
        'div',
        { className: 'flex-ns' },
        _react2.default.createElement(
          'div',
          { className: 'w-100 w-30-l vh-50 vh-100-ns pa3 flex flex-column' },
          _react2.default.createElement(
            'div',
            { className: 'h-50' },
            _react2.default.createElement(_CodeInput2.default, {
              value: this.state.code,
              onChange: this.handleCodeUpdate
            })
          ),
          _react2.default.createElement(
            'div',
            { className: 'h-50' },
            _react2.default.createElement(_CodePreview2.default, {
              code: this.state.code
            })
          )
        ),
        _react2.default.createElement(
          'div',
          { className: 'w-100 w-70-l vh-50 vh-100-ns pa3' },
          _react2.default.createElement(_Visuals2.default, { code: this.state.code })
        )
      );
    }
  }]);

  return App;
}(_react2.default.Component);

exports.default = App;