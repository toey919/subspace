'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _classnames = require('classnames');

var _classnames2 = _interopRequireDefault(_classnames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Tab = function Tab(props) {
  return (
    // eslint-disable-next-line
    _react2.default.createElement(
      'div',
      {
        className: (0, _classnames2.default)('pv3 pr3', { underline: !props.selected, pointer: !props.selected }),
        onClick: function onClick() {
          return props.onClick(props.id);
        }
      },
      props.label
    )
  );
};

Tab.propTypes = {
  id: _propTypes2.default.any.isRequired,
  label: _propTypes2.default.string.isRequired,
  onClick: _propTypes2.default.func.isRequired,
  selected: _propTypes2.default.bool.isRequired
};

exports.default = Tab;