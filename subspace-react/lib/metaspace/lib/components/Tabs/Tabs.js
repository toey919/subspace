'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _ramda = require('ramda');

var _Tab = require('./Tab');

var _Tab2 = _interopRequireDefault(_Tab);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Tabs = function Tabs(props) {
  var ids = props.ids.length === 0 ? (0, _ramda.range)(0, props.labels.length) : props.ids;
  return _react2.default.createElement(
    'div',
    { className: 'flex' },
    (0, _ramda.zipWith)(function (id, label) {
      return { id: id, label: label };
    }, ids, props.labels).map(function (_ref) {
      var id = _ref.id,
          label = _ref.label;
      return _react2.default.createElement(_Tab2.default, {
        id: id,
        label: label,
        selected: props.selected === id,
        onClick: props.onClick
      });
    })
  );
};

Tabs.defaultProps = {
  ids: []
};

Tabs.propTypes = {
  labels: _propTypes2.default.arrayOf(_propTypes2.default.string).isRequired,
  ids: _propTypes2.default.array,
  // eslint-disable-next-line
  selected: _propTypes2.default.any.isRequired,
  onClick: _propTypes2.default.func.isRequired
};

exports.default = Tabs;