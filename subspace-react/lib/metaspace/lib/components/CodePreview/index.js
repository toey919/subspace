'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _CodePreview = require('./CodePreview');

Object.defineProperty(exports, 'default', {
  enumerable: true,
  get: function get() {
    return _interopRequireDefault(_CodePreview).default;
  }
});

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }