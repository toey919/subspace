'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CodeInput = function CodeInput(props) {
  return _react2.default.createElement(
    'div',
    { className: 'h-100 flex flex-column' },
    _react2.default.createElement(
      'h3',
      null,
      'Code Input'
    ),
    _react2.default.createElement('textarea', {
      value: props.value,
      onChange: function onChange(e) {
        return props.onChange(e.target.value);
      },
      className: 'w-100 flex-grow'
    })
  );
};

CodeInput.propTypes = {
  value: _propTypes2.default.string.isRequired,
  onChange: _propTypes2.default.func.isRequired
};

exports.default = CodeInput;