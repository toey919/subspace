'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRelay = require('react-relay');

var _ramda = require('ramda');

var _rxjs = require('rxjs');

var _d = require('d3');

var d3 = _interopRequireWildcard(_d);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /* eslint-disable no-param-reassign */
/* eslint-disable no-return-assign */
/* eslint-disable no-multi-assign */
/* eslint-disable class-methods-use-this */

var css = {
  'node': 'Connectivity__node___BCPH4',
  'link': 'Connectivity__link___1LVH8',
  'node--source': 'Connectivity__node--source___2fLay',
  'node--target': 'Connectivity__node--target___3FeXM',
  'link--source': 'Connectivity__link--source___yxBvc',
  'link--target': 'Connectivity__link--target___3vTfz'
};

var Connectivity = function (_React$Component) {
  _inherits(Connectivity, _React$Component);

  function Connectivity() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, Connectivity);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = Connectivity.__proto__ || Object.getPrototypeOf(Connectivity)).call.apply(_ref, [this].concat(args))), _this), _this.mouseovered = function (d) {
      _this.nodes.each(function (n) {
        return n.target = n.source = false;
      });

      _this.links.classed(css['link--target'], function (l) {
        return l.target === d ? l.source.source = true : null;
      }).classed(css['link--source'], function (l) {
        return l.source === d ? l.target.target = true : null;
      }).filter(function (l) {
        return l.target === d || l.source === d;
      }).raise();

      _this.nodes.classed(css['node--target'], (0, _ramda.prop)('target')).classed(css['node--source'], (0, _ramda.prop)('source'));
    }, _this.mouseouted = function () {
      _this.links.classed(css['link--target'], false).classed(css['link--source'], false);

      _this.nodes.classed(css['node--target'], false).classed(css['node--source'], false);
    }, _this.renderLayout = function () {
      var _this$container = _this.container,
          width = _this$container.offsetWidth,
          height = _this$container.offsetHeight;

      var radius = Math.min(width, height) / 2;
      var innerRadius = radius * 0.7;

      _this.svg.attr('width', radius * 2).attr('height', radius * 2).select('#container').attr('transform', 'translate(' + radius + ', ' + radius + ')');

      // Build d3 hierarchy based on class inheritance relationships
      var root = _this.getHierarchy();

      // Build a cluster (a tree where all leaves are on the same level)
      // This function adds x, y coordinates to each node (x ranging in 0 - 360, y in 0 - innerRadius)
      // So that each node represent a leaf / branch of a cluster
      d3.cluster().size([360, innerRadius])(root);

      // We are using a trick in a hierarchy so that we duplicate each branch (node that has children)
      // so that this branch will also have a clone leaf. 
      // That way root.leaves() contains every class with different x - coordinate, ranging from
      // 0 to 360. Close classes in terms of hierarchy have close x coordinate
      var nodesData = (0, _ramda.filter)(function (node) {
        return node.data.uid !== 0;
      })(root.leaves());

      // Here we simply map our x 0 to 360 coordinate to angle on a circle
      if (_this.nodes) _this.nodes.remove();
      _this.nodes = _this.nodesContainer.selectAll(css.node).data(nodesData).enter().append('text').text(function (d) {
        return d.data.name;
      }).attr('class', css.node).attr('dy', '0.31em').on('mouseover', _this.mouseovered).on('mouseout', _this.mouseouted);

      _this.nodes.attr('transform', function (d) {
        return 'rotate(' + (d.x - 90) + ')translate(' + d.y + ', 0)' + (d.x < 180 ? '' : 'rotate(180)');
      }).attr('text-anchor', function (d) {
        return d.x < 180 ? 'start' : 'end';
      });

      // We can supply to line a set of objects with x, y coordinates
      // and it will draw the splince through this points. Note that we convert
      // x to polar coordinates as well
      var line = d3.lineRadial().radius((0, _ramda.prop)('y')).angle(function (d) {
        return d.x / 180 * Math.PI;
      }).curve(d3.curveBundle.beta(0.85));
      // Path function returns path in a hierachy from one node to another
      // That way we kind of draw the line through parents in a tree
      // (which remains invisible nodes, as we used root.leaves())
      var curvedPath = function curvedPath(d) {
        return line(d.source.path(d.target));
      };

      var linksData = (0, _ramda.map)(function (edge) {
        return {
          source: (0, _ramda.find)((0, _ramda.pathEq)(['data', 'uid'], edge.from.uid))(nodesData),
          target: (0, _ramda.find)((0, _ramda.pathEq)(['data', 'uid'], edge.to.uid))(nodesData) };
      })(_this.props.connectivity);

      if (_this.links) _this.links.remove();
      _this.links = _this.linksContainer.selectAll(css.link).data(linksData).enter().append('path').attr('class', css.link).attr('d', curvedPath);
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(Connectivity, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      this.svg = d3.select(this.container).append('svg');
      this.svg.append('g').attr('id', 'container');
      this.nodesContainer = this.svg.select('#container').append('g').attr('id', 'nodes');
      this.linksContainer = this.svg.select('#container').append('g').attr('id', 'links');
      this.renderLayout();
      this.resizeSubscription = _rxjs.Observable.fromEvent(window, 'resize').debounceTime(500).subscribe(this.renderLayout);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.resizeSubscription.unsubscribe();
    }

    /**
     * Builds a d3 hierarchy from graph. While in reality we can have many parents,
     * here we reduce many parents to one, using resolveParent function.
     * 
     * The other trick is we have to clone each non-leaf node so that it will be leaf.
     * This is needed in order to make all classes appear as nodes on one level, not
     * intersecting.
     */

  }, {
    key: 'getHierarchy',
    value: function getHierarchy() {
      // Create a hashmap of classes for quick access by uid
      var classes = (0, _ramda.pipe)((0, _ramda.map)((0, _ramda.prop)('vertex')), (0, _ramda.map)(function (vertex) {
        return _extends({}, vertex, { parents: [] });
      }), (0, _ramda.groupBy)((0, _ramda.prop)('uid')), (0, _ramda.map)(_ramda.head))(this.props.hierarchy.layeredVertices);

      // fill parents
      this.props.hierarchy.edges.forEach(function (_ref2) {
        var from = _ref2.from,
            to = _ref2.to;

        classes[to.uid].parents.push(classes[from.uid]);
      });
      var classesList = (0, _ramda.values)(classes);
      // Global parent is a requirement of d3, we need it since we have many roots
      var globalParent = { uid: 0, name: 'global', type: 'global' };
      // transform many parents to one, add global parent where necessary
      var stratifiedClasses = (0, _ramda.pipe)((0, _ramda.map)(this.resolveParents), (0, _ramda.zip)(classesList), (0, _ramda.map)(function (classAndParent) {
        return _extends({}, classAndParent[0], { parent: classAndParent[1] });
      }), (0, _ramda.map)((0, _ramda.dissoc)('parents')), (0, _ramda.map)(function (cls) {
        return cls.parent.uid ? cls : _extends({}, cls, { parent: globalParent });
      }))(classesList);

      // each clone needs an id - this is id generator
      var maxUid = (0, _ramda.pipe)(_ramda.keys, (0, _ramda.map)(function (x) {
        return parseInt(x, 10);
      }), (0, _ramda.reduce)(_ramda.max)(0))(classes);
      var newUid = function () {
        var current = maxUid + 1;
        // eslint-disable-next-line no-plusplus
        return function () {
          return current++;
        };
      }();

      var branchClones = (0, _ramda.pipe)((0, _ramda.map)((0, _ramda.path)(['parent', 'uid'])), (0, _ramda.filter)(_ramda.identity), _ramda.uniq, (0, _ramda.map)(function (uid) {
        return (0, _ramda.find)((0, _ramda.propEq)('uid', uid))(stratifiedClasses);
      }), (0, _ramda.map)(function (cls) {
        var cloned = (0, _ramda.pipe)(_ramda.clone, (0, _ramda.dissoc)('parent'))(cls);
        var stratifiedCls = (0, _ramda.find)((0, _ramda.propEq)('uid', cls.uid))(stratifiedClasses);
        // note that we need old uid to be in a leaf, so our class interconnections work
        // correctly. Non-leaf will have new id and used only for line drawing.
        stratifiedCls.uid = newUid();
        cloned.parent = stratifiedCls;
        // update connections to old node with new id
        stratifiedClasses.forEach(function (klass) {
          if (klass.parent.uid === cloned.uid) klass.parent.uid = stratifiedCls.uid;
        });
        return cloned;
      }))(stratifiedClasses);
      var allClasses = [].concat(_toConsumableArray(stratifiedClasses), _toConsumableArray(branchClones), [globalParent]);
      var stratify = d3.stratify().id((0, _ramda.prop)('uid')).parentId((0, _ramda.path)(['parent', 'uid']));
      return stratify(allClasses);
    }
  }, {
    key: 'resolveParents',


    /**
     * Though we can have multiple parents, to make visualization
     * we need to reduce multiple parents to one. This is done according
     * to priorities - obj has the highest priority, interface - the lowest.
     */
    value: function resolveParents(cls) {
      var scores = {
        obj: 4,
        class: 3,
        trait: 2,
        interface: 1
      };
      var comparator = function comparator(a, b) {
        return -((scores[a.type] || 0) - (scores[b.type] || 0));
      };
      return (0, _ramda.pipe)((0, _ramda.sort)(comparator), _ramda.head, (0, _ramda.dissoc)('parents'))(cls.parents);
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement('div', { className: 'flex-grow f3', ref: function ref(container) {
          _this2.container = container;
        } });
    }
  }]);

  return Connectivity;
}(_react2.default.Component);

Connectivity.propTypes = {
  connectivity: _propTypes2.default.object.isRequired,
  hierarchy: _propTypes2.default.object.isRequired
};

exports.default = (0, _reactRelay.createFragmentContainer)(Connectivity, {
  connectivity: function connectivity() {
    return require('./__generated__/Connectivity_connectivity.graphql');
  },
  hierarchy: function hierarchy() {
    return require('./__generated__/Connectivity_hierarchy.graphql');
  }
});