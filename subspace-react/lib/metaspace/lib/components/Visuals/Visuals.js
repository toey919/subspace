'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _graphql;

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRelay = require('react-relay');

var _environment = require('../../relay/environment');

var _environment2 = _interopRequireDefault(_environment);

var _Tabs = require('../Tabs');

var _Tabs2 = _interopRequireDefault(_Tabs);

var _ClassDiagram = require('../ClassDiagram');

var _ClassDiagram2 = _interopRequireDefault(_ClassDiagram);

var _Sequence = require('../Sequence');

var _Sequence2 = _interopRequireDefault(_Sequence);

var _Connectivity = require('../Connectivity');

var _Connectivity2 = _interopRequireDefault(_Connectivity);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Visuals = function (_React$Component) {
  _inherits(Visuals, _React$Component);

  function Visuals(props) {
    _classCallCheck(this, Visuals);

    var _this = _possibleConstructorReturn(this, (Visuals.__proto__ || Object.getPrototypeOf(Visuals)).call(this, props));

    _this.renderComponent = function () {
      switch (_this.state.selected) {
        case 'classDiagram':
          return _react2.default.createElement(_ClassDiagram2.default, { classDiagram: _this.props.classDiagram });
        case 'sequence':
          return _react2.default.createElement(_Sequence2.default, { sequence: _this.props.sequence });
        case 'connectivity':
          return _react2.default.createElement(_Connectivity2.default, {
            connectivity: _this.props.connectivity,
            hierarchy: _this.props.hierarchy
          });
        default:
          return null;
      }
    };

    _this.state = { selected: 'connectivity' };
    return _this;
  }

  _createClass(Visuals, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement(
        'div',
        { className: 'flex flex-column h-100' },
        _react2.default.createElement(
          'h3',
          null,
          'Visuals'
        ),
        _react2.default.createElement(_Tabs2.default, {
          labels: ['Class diagram', 'Sequence diagram', 'Connectivity'],
          ids: ['classDiagram', 'sequence', 'connectivity'],
          onClick: function onClick(selected) {
            return _this2.setState({ selected: selected });
          },
          selected: this.state.selected
        }),
        this.renderComponent()
      );
    }
  }]);

  return Visuals;
}(_react2.default.Component);

Visuals.defaultProps = {
  classDiagram: {},
  sequence: {},
  connectivity: {},
  hierarchy: {}
};

Visuals.propTypes = {
  classDiagram: _propTypes2.default.object,
  sequence: _propTypes2.default.object,
  connectivity: _propTypes2.default.object,
  hierarchy: _propTypes2.default.object
};

exports.default = function (ownProps) {
  return _react2.default.createElement(_reactRelay.QueryRenderer, {
    environment: _environment2.default,
    query: _graphql || (_graphql = function _graphql() {
      return require('./__generated__/VisualsQuery.graphql');
    }),
    variables: {
      code: ownProps.code
    },
    render: function render(_ref) {
      var error = _ref.error,
          props = _ref.props;

      if (error) {
        throw error;
      } else if (props) {
        return _react2.default.createElement(Visuals, props);
      }
      return _react2.default.createElement(
        'div',
        null,
        'Loading'
      );
    }
  });
};