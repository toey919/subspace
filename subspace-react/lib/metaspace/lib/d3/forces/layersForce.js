'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _ramda = require('ramda');

/**
 * Pulls nodes towards a predefined layer
 */
exports.default = function () {
  var strength = void 0;
  var height = void 0;
  var layerPropName = void 0;
  var nodes = void 0;
  var yStep = void 0;
  var strengths = void 0;

  function force(alpha) {
    nodes.forEach(function (node, i) {
      node.y += (node.layer * yStep - node.y) * strengths[i] * alpha;
    });
  }

  function initialize() {
    if (!nodes) return;

    // populate local `strengths` using `strength` accessor
    // basically strenght can be a function of 3 variables - node, i, nodes
    // e.g. layerForce().strength((node, i, nodes) => ...)
    strengths = nodes.map(function (node, i) {
      return strength(node, i, nodes);
    });
    var layersCount = (0, _ramda.pipe)((0, _ramda.map)((0, _ramda.prop)(layerPropName())), _ramda.uniq, (0, _ramda.reduce)(_ramda.max, 0))(nodes);
    yStep = height() / layersCount;
  }

  force.initialize = function (initialNodes) {
    nodes = initialNodes;
    initialize();
  };

  // if called with zero args with get strength
  // if calls with args - will set strength
  force.strength = function (_) {
    // return existing value if no value passed
    if (_ == null) return strength;

    // coerce `strength` accessor into a function
    strength = typeof _ === 'function' ? _ : function () {
      return +_;
    };

    // reinitialize
    initialize();

    // allow chaining
    return force;
  };

  force.height = function (_) {
    if (_ == null) return height;
    height = typeof _ === 'function' ? _ : function () {
      return +_;
    };
    initialize();
    return force;
  };

  force.layerPropName = function (_) {
    if (_ == null) return height;
    layerPropName = typeof _ === 'function' ? _ : function () {
      return _;
    };
    initialize();
    return force;
  };

  if (!strength) force.strength(0.1);
  if (!height) force.height(400);
  if (!layerPropName) force.layerPropName('layer');

  return force;
}; /* eslint-disable no-param-reassign */