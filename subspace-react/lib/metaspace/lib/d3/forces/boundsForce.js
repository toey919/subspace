'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

/* eslint-disable no-mixed-operators */
/* eslint-disable no-param-reassign */

/**
 * Pulls nodes towards a predefined layer
 */
exports.default = function () {
  var strength = void 0;
  var width = void 0;
  var height = void 0;
  var nodes = void 0;
  var strengths = void 0;

  function force(alpha) {
    // factor to normalize force from 0 to 1
    var factor = 10000;
    nodes.forEach(function (node, i) {
      var charLength = 9; // approximate for font size 18  
      var length = node.name.length * charLength;
      var x = Math.max(1, Math.min(width() - 1, node.x));
      var y = Math.max(1, Math.min(height() - 1, node.y));
      var xForce = alpha * strengths[i] / x - alpha * strengths[i] / Math.max(width() - x - length, 1);
      var yForce = alpha * strengths[i] / y - alpha * strengths[i] / (height() - y);
      node.vx += xForce * factor;
      node.vy += yForce * factor;
    });
  }

  function initialize() {
    if (!nodes) return;

    // populate local `strengths` using `strength` accessor
    // basically strenght can be a function of 3 variables - node, i, nodes
    // e.g. layerForce().strength((node, i, nodes) => ...)
    strengths = nodes.map(function (node, i) {
      return strength(node, i, nodes);
    });
  }

  force.initialize = function (initialNodes) {
    nodes = initialNodes;
    initialize();
  };

  // if called with zero args with get strength
  // if calls with args - will set strength
  force.strength = function (_) {
    // return existing value if no value passed
    if (_ == null) return strength;

    // coerce `strength` accessor into a function
    strength = typeof _ === 'function' ? _ : function () {
      return +_;
    };

    // reinitialize
    initialize();

    // allow chaining
    return force;
  };

  force.width = function (_) {
    if (_ == null) return height;
    width = typeof _ === 'function' ? _ : function () {
      return _;
    };
    initialize();
    return force;
  };

  force.height = function (_) {
    if (_ == null) return height;
    height = typeof _ === 'function' ? _ : function () {
      return +_;
    };
    initialize();
    return force;
  };

  if (!strength) force.strength(0.1);
  if (!width) force.width(400);
  if (!height) force.height(400);

  return force;
};