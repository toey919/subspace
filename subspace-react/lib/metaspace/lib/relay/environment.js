'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _relayRuntime = require('relay-runtime');

var source = new _relayRuntime.RecordSource();
var store = new _relayRuntime.Store(source);
var handlerProvider = null;

function fetchQuery(operation, variables) {
  var body = {
    operationName: operation.name,
    query: operation.text,
    variables: JSON.stringify(variables)
  };
  var options = {
    method: 'POST',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(body)
  };
  return fetch(__METASPACE_GRAPHQL_ENDPOINT__, options).then(function (response) {
    return response.json();
  });
}
var network = _relayRuntime.Network.create(fetchQuery);

exports.default = new _relayRuntime.Environment({
  handlerProvider: handlerProvider,
  network: network,
  store: store
});