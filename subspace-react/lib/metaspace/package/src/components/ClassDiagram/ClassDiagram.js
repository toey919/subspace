/* eslint-disable no-param-reassign */
/* eslint-disable no-return-assign */
/* eslint-disable no-multi-assign */
/* eslint-disable class-methods-use-this */

import React from 'react';
import PropTypes from 'prop-types';
import { createFragmentContainer, graphql } from 'react-relay';
import { map, prop, propEq, path, findIndex } from 'ramda';
import { Observable } from 'rxjs';
import * as d3 from 'd3';

import { layersForce, boundsForce } from '../../d3/forces';
import css from './ClassDiagram.scss';

class ClassDiagram extends React.Component {
  componentDidMount() {
    const svg = d3.select(this.container).append('svg')
      .attr('class', 'f3');

    svg.append('defs').selectAll(`.${css.marker}`)
      .data(['marker'])
      .enter()
      .append('marker')
      .attr('id', d => d)
      .attr('viewBox', '0 -5 10 10')
      .attr('refX', 15)
      .attr('markerWidth', 5)
      .attr('markerHeight', 5)
      .attr('orient', 'auto')
      .attr('class', css.marker)
      .append('path')
      .attr('d', 'M0,-5L10,0L0,5');

    svg.append('g').attr('id', 'text');
    svg.append('g').attr('id', 'node');
    svg.append('g').attr('id', 'link');

    this.startSimulation();
    this.resizeSubscription =
      Observable.fromEvent(window, 'resize')
        .debounceTime(200)
        .subscribe(this.updateSumulation);
  }

  componentWillUnmount() {
    this.resizeSubscription.unsubscribe();
  }

  updateSumulation = () => {
    const { offsetWidth: width, offsetHeight: height } = this.container;
    d3.select(this.container).select('svg')
      .attr('width', width)
      .attr('height', height);
    this.simulation.force('center').x(width / 2).y(height / 2);
    this.simulation.force('layer').height(height);
    this.simulation.force('bounds').width(width).height(height);
    this.simulation.alpha(1).restart();
  }

  startSimulation = () => {
    const { offsetWidth: width, offsetHeight: height } = this.container;
    const { layeredVertices, edges } = this.props.classDiagram;
    const nodes = layeredVertices.map(
      layeredVertex => ({ ...layeredVertex.vertex, layer: layeredVertex.layer }),
    );
    const links = map(edge => ({ source: findIndex(propEq('uid', edge.from.uid))(nodes), target: findIndex(propEq('uid', edge.to.uid))(nodes) }), edges);
    this.simulation = d3.forceSimulation(nodes)
      .force('link', d3.forceLink(links).distance(90))
      .force('center', d3.forceCenter(width / 2, height / 2))
      .force('charge', d3.forceManyBody().strength(-1800))
      .force('layer', layersForce().height(height).strength(1.2))
      .force('bounds', boundsForce().width(width).height(height).strength(0.1));

    const dragstarted = (d) => {
      if (!d3.event.active) this.simulation.alphaTarget(0.3).restart();
      d.fx = d.x;
      d.fy = d.y;
    };

    const dragged = (d) => {
      d.fx = d3.event.x;
      d.fy = d3.event.y;
    };

    const dragended = (d) => {
      if (!d3.event.active) this.simulation.alphaTarget(0);
      d.fx = null;
      d.fy = null;
    };

    const svg = d3.select(this.container).select('svg')
      .attr('width', width)
      .attr('height', height);

    const link = svg.select('g#link').selectAll(`.${css.link}`)
      .data(links)
      .enter()
      .append('line')
      .attr('class', css.link)
      .attr('marker-end', 'url(#marker)');

    const node = svg.select('g#node').selectAll(`.${css.node}`)
      .data(nodes)
      .enter()
      .append('circle')
      .attr('class', css.node)
      .attr('r', 5)
      .call(d3.drag()
        .on('start', dragstarted)
        .on('drag', dragged)
        .on('end', dragended));

    const text = svg.select('g#text').selectAll(`.${css.text}`)
      .data(nodes)
      .enter()
      .append('text')
      .attr('class', css.text)
      .attr('x', 8)
      .attr('y', '.31em')
      .text(prop('name'))
      .call(d3.drag()
        .on('start', dragstarted)
        .on('drag', dragged)
        .on('end', dragended));


    const transform = d => `translate(${d.x},${d.y})`;

    this.simulation.on('tick', () => {
      node.attr('transform', transform);
      text.attr('transform', transform);

      link
        .attr('x1', path(['source', 'x']))
        .attr('y1', path(['source', 'y']))
        .attr('x2', path(['target', 'x']))
        .attr('y2', path(['target', 'y']));
    });
  }

  render() {
    return (
      <div className="flex-grow" ref={(container) => { this.container = container; }} />
    );
  }
}


ClassDiagram.propTypes = {
  classDiagram: PropTypes.shape({
    layeredVertices: PropTypes.arrayOf({
      layer: PropTypes.number.isRequired,
      vertex: PropTypes.shape({
        uid: PropTypes.number.isRequired,
        name: PropTypes.string.isRequired,
        type: PropTypes.string.isRequired,
      }).isRequired,
    }).isRequired,
    edges: PropTypes.arrayOf(PropTypes.shape({
      from: PropTypes.any.isRequired,
      to: PropTypes.any.isRequired }),
    ).isRequired,
  }).isRequired,
};

export default createFragmentContainer(
  ClassDiagram,
  graphql`
    fragment ClassDiagram_classDiagram on LayeredGraph {
      layeredVertices {
        layer
        vertex {
          uid
          name
          type        
        }
      }
      edges {
        from {
          uid
        }
        to {
          uid
        }
      }
    }
  `,
);
