import React from 'react';
import PropTypes from 'prop-types';
import { QueryRenderer, graphql } from 'react-relay';

import environment from '../../relay/environment';
import Tabs from '../Tabs';
import ClassDiagram from '../ClassDiagram';
import Sequence from '../Sequence';
import Connectivity from '../Connectivity';

class Visuals extends React.Component {
  constructor(props) {
    super(props);
    this.state = { selected: 'connectivity' };
  }

  renderComponent = () => {
    switch (this.state.selected) {
      case 'classDiagram':
        return <ClassDiagram classDiagram={this.props.classDiagram} />;
      case 'sequence':
        return <Sequence sequence={this.props.sequence} />;
      case 'connectivity':
        return (
          <Connectivity
            connectivity={this.props.connectivity}
            hierarchy={this.props.hierarchy}
          />
        );
      default:
        return null;
    }
  }

  render() {
    return (
      <div className="flex flex-column h-100">
        <h3>Visuals</h3>
        <Tabs
          labels={['Class diagram', 'Sequence diagram', 'Connectivity']}
          ids={['classDiagram', 'sequence', 'connectivity']}
          onClick={selected => this.setState({ selected })}
          selected={this.state.selected}
        />
        {this.renderComponent()}
      </div>
    );
  }
}

Visuals.defaultProps = {
  classDiagram: {},
  sequence: {},
  connectivity: {},
  hierarchy: {},
};

Visuals.propTypes = {
  classDiagram: PropTypes.object,
  sequence: PropTypes.object,
  connectivity: PropTypes.object,
  hierarchy: PropTypes.object,
};

export default ownProps => (
  <QueryRenderer
    environment={environment}
    query={graphql`
      query VisualsQuery($code: String!) {
        classDiagram(code: $code) {
          ...ClassDiagram_classDiagram
        }
        sequence(code: $code) {
          ...Sequence_sequence
        }
        connectivity(code: $code) {
          ...Connectivity_connectivity
        }
        hierarchy: classDiagram(code: $code) {
          ...Connectivity_hierarchy
        }
      }
    `}
    variables={{
      code: ownProps.code,
    }}
    render={({ error, props }) => {
      if (error) {
        throw error;
      } else if (props) {
        return <Visuals {...props} />;
      }
      return <div>Loading</div>;
    }}
  />
);
