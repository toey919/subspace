import React from 'react';

import CodeInput from './CodeInput';
import CodePreview from './CodePreview';
import Visuals from './Visuals';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.localStorage = window ? window.localStorage : { getItem: () => {}, setItem: () => {} };
    this.state = { code: this.localStorage.getItem('code') || '' };
  }

  handleCodeUpdate = (code) => {
    this.setState({ code });
    this.localStorage.setItem('code', code);
  }

  render() {
    return (
      <div className="flex-ns">
        <div className="w-100 w-30-l vh-50 vh-100-ns pa3 flex flex-column">
          <div className="h-50">
            <CodeInput
              value={this.state.code}
              onChange={this.handleCodeUpdate}
            />
          </div>
          <div className="h-50">
            <CodePreview
              code={this.state.code}
            />
          </div>
        </div>
        <div className="w-100 w-70-l vh-50 vh-100-ns pa3">
          <Visuals code={this.state.code} />
        </div>
      </div>
    );
  }
}

export default App;
