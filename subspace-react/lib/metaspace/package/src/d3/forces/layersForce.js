/* eslint-disable no-param-reassign */

import { map, prop, pipe, uniq, max, reduce } from 'ramda';

/**
 * Pulls nodes towards a predefined layer
 */
export default () => {
  let strength;
  let height;
  let layerPropName;
  let nodes;
  let yStep;
  let strengths;

  function force(alpha) {
    nodes.forEach((node, i) => {
      node.y += ((node.layer * yStep) - node.y) * strengths[i] * alpha;
    });
  }

  function initialize() {
    if (!nodes) return;

    // populate local `strengths` using `strength` accessor
    // basically strenght can be a function of 3 variables - node, i, nodes
    // e.g. layerForce().strength((node, i, nodes) => ...)
    strengths = nodes.map((node, i) => strength(node, i, nodes));
    const layersCount = pipe(
      map(prop(layerPropName())),
      uniq,
      reduce(max, 0),
    )(nodes);
    yStep = height() / layersCount;
  }

  force.initialize = (initialNodes) => {
    nodes = initialNodes;
    initialize();
  };

  // if called with zero args with get strength
  // if calls with args - will set strength
  force.strength = (_) => {
    // return existing value if no value passed
    if (_ == null) return strength;

    // coerce `strength` accessor into a function
    strength = typeof _ === 'function' ? _ : () => +_;

    // reinitialize
    initialize();

    // allow chaining
    return force;
  };

  force.height = (_) => {
    if (_ == null) return height;
    height = typeof _ === 'function' ? _ : () => +_;
    initialize();
    return force;
  };

  force.layerPropName = (_) => {
    if (_ == null) return height;
    layerPropName = typeof _ === 'function' ? _ : () => _;
    initialize();
    return force;
  };

  if (!strength) force.strength(0.1);
  if (!height) force.height(400);
  if (!layerPropName) force.layerPropName('layer');

  return force;
};
