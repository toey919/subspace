import { Environment, Network, RecordSource, Store } from 'relay-runtime';

const source = new RecordSource();
const store = new Store(source);
const handlerProvider = null;

function fetchQuery(
  operation,
  variables,
) {
  const body = {
    operationName: operation.name,
    query: operation.text,
    variables: JSON.stringify(variables),
  };
  const options = {
    method: 'POST',
    headers: {
      'content-type': 'application/json',
    },
    body: JSON.stringify(body),
  };
  return fetch(__METASPACE_GRAPHQL_ENDPOINT__, options)
    .then(response => response.json());
}
const network = Network.create(fetchQuery);


export default new Environment({
  handlerProvider,
  network,
  store,
});
