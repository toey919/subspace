'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRelay = require('react-relay');

var _ramda = require('ramda');

var _rxjs = require('rxjs');

var _d = require('d3');

var d3 = _interopRequireWildcard(_d);

var _forces = require('../../d3/forces');

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; } /* eslint-disable no-param-reassign */
/* eslint-disable no-return-assign */
/* eslint-disable no-multi-assign */
/* eslint-disable class-methods-use-this */

var css = {
  'node': 'ClassDiagram__node___1J0BZ',
  'link': 'ClassDiagram__link___1Yues',
  'marker': 'ClassDiagram__marker___23r3x',
  'text': 'ClassDiagram__text___2lNYe'
};

var ClassDiagram = function (_React$Component) {
  _inherits(ClassDiagram, _React$Component);

  function ClassDiagram() {
    var _ref;

    var _temp, _this, _ret;

    _classCallCheck(this, ClassDiagram);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    return _ret = (_temp = (_this = _possibleConstructorReturn(this, (_ref = ClassDiagram.__proto__ || Object.getPrototypeOf(ClassDiagram)).call.apply(_ref, [this].concat(args))), _this), _this.updateSumulation = function () {
      var _this$container = _this.container,
          width = _this$container.offsetWidth,
          height = _this$container.offsetHeight;

      d3.select(_this.container).select('svg').attr('width', width).attr('height', height);
      _this.simulation.force('center').x(width / 2).y(height / 2);
      _this.simulation.force('layer').height(height);
      _this.simulation.force('bounds').width(width).height(height);
      _this.simulation.alpha(1).restart();
    }, _this.startSimulation = function () {
      var _this$container2 = _this.container,
          width = _this$container2.offsetWidth,
          height = _this$container2.offsetHeight;
      var _this$props$classDiag = _this.props.classDiagram,
          layeredVertices = _this$props$classDiag.layeredVertices,
          edges = _this$props$classDiag.edges;

      var nodes = layeredVertices.map(function (layeredVertex) {
        return _extends({}, layeredVertex.vertex, { layer: layeredVertex.layer });
      });
      var links = (0, _ramda.map)(function (edge) {
        return { source: (0, _ramda.findIndex)((0, _ramda.propEq)('uid', edge.from.uid))(nodes), target: (0, _ramda.findIndex)((0, _ramda.propEq)('uid', edge.to.uid))(nodes) };
      }, edges);
      _this.simulation = d3.forceSimulation(nodes).force('link', d3.forceLink(links).distance(90)).force('center', d3.forceCenter(width / 2, height / 2)).force('charge', d3.forceManyBody().strength(-1800)).force('layer', (0, _forces.layersForce)().height(height).strength(1.2)).force('bounds', (0, _forces.boundsForce)().width(width).height(height).strength(0.1));

      var dragstarted = function dragstarted(d) {
        if (!d3.event.active) _this.simulation.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
      };

      var dragged = function dragged(d) {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
      };

      var dragended = function dragended(d) {
        if (!d3.event.active) _this.simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
      };

      var svg = d3.select(_this.container).select('svg').attr('width', width).attr('height', height);

      var link = svg.select('g#link').selectAll('.' + css.link).data(links).enter().append('line').attr('class', css.link).attr('marker-end', 'url(#marker)');

      var node = svg.select('g#node').selectAll('.' + css.node).data(nodes).enter().append('circle').attr('class', css.node).attr('r', 5).call(d3.drag().on('start', dragstarted).on('drag', dragged).on('end', dragended));

      var text = svg.select('g#text').selectAll('.' + css.text).data(nodes).enter().append('text').attr('class', css.text).attr('x', 8).attr('y', '.31em').text((0, _ramda.prop)('name')).call(d3.drag().on('start', dragstarted).on('drag', dragged).on('end', dragended));

      var transform = function transform(d) {
        return 'translate(' + d.x + ',' + d.y + ')';
      };

      _this.simulation.on('tick', function () {
        node.attr('transform', transform);
        text.attr('transform', transform);

        link.attr('x1', (0, _ramda.path)(['source', 'x'])).attr('y1', (0, _ramda.path)(['source', 'y'])).attr('x2', (0, _ramda.path)(['target', 'x'])).attr('y2', (0, _ramda.path)(['target', 'y']));
      });
    }, _temp), _possibleConstructorReturn(_this, _ret);
  }

  _createClass(ClassDiagram, [{
    key: 'componentDidMount',
    value: function componentDidMount() {
      var svg = d3.select(this.container).append('svg').attr('class', 'f3');

      svg.append('defs').selectAll('.' + css.marker).data(['marker']).enter().append('marker').attr('id', function (d) {
        return d;
      }).attr('viewBox', '0 -5 10 10').attr('refX', 15).attr('markerWidth', 5).attr('markerHeight', 5).attr('orient', 'auto').attr('class', css.marker).append('path').attr('d', 'M0,-5L10,0L0,5');

      svg.append('g').attr('id', 'text');
      svg.append('g').attr('id', 'node');
      svg.append('g').attr('id', 'link');

      this.startSimulation();
      this.resizeSubscription = _rxjs.Observable.fromEvent(window, 'resize').debounceTime(200).subscribe(this.updateSumulation);
    }
  }, {
    key: 'componentWillUnmount',
    value: function componentWillUnmount() {
      this.resizeSubscription.unsubscribe();
    }
  }, {
    key: 'render',
    value: function render() {
      var _this2 = this;

      return _react2.default.createElement('div', { className: 'flex-grow', ref: function ref(container) {
          _this2.container = container;
        } });
    }
  }]);

  return ClassDiagram;
}(_react2.default.Component);

ClassDiagram.propTypes = {
  classDiagram: _propTypes2.default.shape({
    layeredVertices: _propTypes2.default.arrayOf({
      layer: _propTypes2.default.number.isRequired,
      vertex: _propTypes2.default.shape({
        uid: _propTypes2.default.number.isRequired,
        name: _propTypes2.default.string.isRequired,
        type: _propTypes2.default.string.isRequired
      }).isRequired
    }).isRequired,
    edges: _propTypes2.default.arrayOf(_propTypes2.default.shape({
      from: _propTypes2.default.any.isRequired,
      to: _propTypes2.default.any.isRequired })).isRequired
  }).isRequired
};

exports.default = (0, _reactRelay.createFragmentContainer)(ClassDiagram, {
  classDiagram: function classDiagram() {
    return require('./__generated__/ClassDiagram_classDiagram.graphql');
  }
});