'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _graphql;

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _reactRelay = require('react-relay');

var _environment = require('../../relay/environment');

var _environment2 = _interopRequireDefault(_environment);

var _CodeElement = require('./CodeElement');

var _CodeElement2 = _interopRequireDefault(_CodeElement);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CodePreview = function CodePreview(props) {
  var elements = props.elements.map(function (element) {
    return element.type === 'call' ? _extends({}, element, { name: element.name.split('|')[1] }) : element;
  });
  return _react2.default.createElement(
    'div',
    null,
    _react2.default.createElement(
      'h3',
      null,
      'Code Preview'
    ),
    _react2.default.createElement(
      'div',
      { style: { whiteSpace: 'pre-wrap' }, className: 'f4' },
      _react2.default.createElement(_CodeElement2.default, {
        uid: props.root.uid,
        depth: 0,
        elements: elements,
        global: true
      })
    )
  );
};

CodePreview.propTypes = {
  elements: _propTypes2.default.arrayOf(_propTypes2.default.shape({
    uid: _propTypes2.default.number.isRequired,
    name: _propTypes2.default.string.isRequired,
    type: _propTypes2.default.string.isRequired,
    children: _propTypes2.default.arrayOf(_propTypes2.default.shape({
      uid: _propTypes2.default.number.isRequired
    })).isRequired
  })).isRequired,
  root: _propTypes2.default.shape({
    uid: _propTypes2.default.string.isRequired
  }).isRequired
};

exports.default = function (ownProps) {
  return _react2.default.createElement(_reactRelay.QueryRenderer, {
    environment: _environment2.default,
    query: _graphql || (_graphql = function _graphql() {
      return require('./__generated__/CodePreviewQuery.graphql');
    }),
    variables: {
      code: ownProps.code
    },
    render: function render(_ref) {
      var error = _ref.error,
          props = _ref.props;

      if (error) {
        return _react2.default.createElement(
          'div',
          null,
          error.message
        );
      } else if (props) {
        return _react2.default.createElement(CodePreview, props);
      }
      return _react2.default.createElement(
        'div',
        null,
        'Loading'
      );
    }
  });
};