'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

var _ramda = require('ramda');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var CodeElement = function (_React$Component) {
  _inherits(CodeElement, _React$Component);

  function CodeElement(props) {
    _classCallCheck(this, CodeElement);

    var _this = _possibleConstructorReturn(this, (CodeElement.__proto__ || Object.getPrototypeOf(CodeElement)).call(this, props));

    _this.state = { expanded: false };
    return _this;
  }

  _createClass(CodeElement, [{
    key: 'render',
    value: function render() {
      var _this2 = this;

      var _find = (0, _ramda.find)((0, _ramda.propEq)('uid', this.props.uid), this.props.elements),
          name = _find.name,
          type = _find.type,
          children = _find.children;

      var cursor = children.length ? 'pointer' : 'default';
      return _react2.default.createElement(
        'div',
        { className: 'pl3' },
        _react2.default.createElement(
          'div',
          {
            role: 'button',
            tabIndex: '0',
            style: { cursor: cursor },
            onClick: function onClick() {
              return _this2.setState({ expanded: !_this2.state.expanded });
            }
          },
          _react2.default.createElement(
            'span',
            null,
            this.props.global ? 'global' : type
          ),
          _react2.default.createElement(
            'span',
            null,
            name ? ' ' + name : ''
          ),
          !!children.length && _react2.default.createElement(
            'span',
            null,
            this.state.expanded ? ' -' : ' +'
          )
        ),
        this.state.expanded && children.map(function (_ref) {
          var uid = _ref.uid;
          return _react2.default.createElement(CodeElement, {
            uid: uid,
            elements: _this2.props.elements
          });
        })
      );
    }
  }]);

  return CodeElement;
}(_react2.default.Component);

CodeElement.defaultProps = {
  global: false
};

CodeElement.propTypes = {
  uid: _propTypes2.default.number.isRequired,
  global: _propTypes2.default.boolean,
  elements: _propTypes2.default.arrayOf(_propTypes2.default.shape({
    name: _propTypes2.default.string,
    type: _propTypes2.default.string.isRequired,
    uid: _propTypes2.default.number.isRequired
  })).isRequired
};

exports.default = CodeElement;