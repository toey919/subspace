const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const isDev = process.env.NODE_ENV === 'dev';
const isDevServer = process.argv.find(v => v.includes('webpack-dev-server'));
const graphQLEndpoint = process.env.METASPACE_GRAPHQL_ENDPOINT || '/graphql';

const namePattern = postfix => (isDev ? `[name].${postfix}` : `[name].min.${postfix}`);

const plugins = [
  new webpack.DefinePlugin({
    __DEV__: JSON.stringify(isDev),
    __RENDER__: JSON.stringify(isDevServer),
    __METASPACE_GRAPHQL_ENDPOINT__: JSON.stringify(graphQLEndpoint),
    'process.env': {
      NODE_ENV: JSON.stringify(isDev ? 'dev' : 'production'),
    },
  }),
  new ExtractTextPlugin({
    filename: isDevServer ? 'stylesheets/[name].css' : namePattern('css'),
    allChunks: true,
  }),
];

if (!isDev) {
  plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compressor: {
        pure_getters: true,
        unsafe: true,
        unsafe_comps: true,
        screw_ie8: true,
        warnings: false,
      },
    })
  );
}

module.exports = {
  entry: {
    app: ['babel-polyfill', 'react-hot-loader/patch', 'tachyons/css/tachyons.css', './src/components/global.css', './src/components/index.js'],
  },
  output: {
    path: path.join(__dirname, 'dist'),
    filename: isDevServer ? 'javascripts/[name].js' : namePattern('js'),
    publicPath: 'http://localhost:8080/assets',
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: {
            loader: 'css-loader',
            options: { minimize: !isDev },
          },
        }),
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: `css-loader?modules&${isDev ? '' : 'minimize'}&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]!sass-loader`,
        }),
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            query: {
              babelrc: false,
              presets: [['es2015', { modules: false }], 'react', 'stage-2'],
              plugins: [
                'react-hot-loader/babel',
                'relay',
              ],
            },
          },
        ],
      },
    ],
  },
  devtool: 'inline-source-map',
  plugins,
  devServer: {
    historyApiFallback: true,
    contentBase: path.resolve(__dirname, 'dist'),
    publicPath: '/assets',

    headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET, POST, PUT, DELETE, PATCH, OPTIONS',
      'Access-Control-Allow-Headers': 'X-Requested-With, content-type, Authorization',
    },

    hot: true,
    // enable HMR on the server
  },
};
