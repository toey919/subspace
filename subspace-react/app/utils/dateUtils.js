import moment from 'moment';

//Calculate the No of days passed based on millisecond value :: (Long) => Int
export const calcDaysDiff = (milliSeconds) => {
  let today = new Date();
  let toCompare = moment(milliSeconds)
  return moment(today).diff(toCompare, 'days');
}