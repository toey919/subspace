import { commitMutation, graphql } from 'react-relay';
import { env } from 'relay/RelayEnvironment';

const mutation = graphql`
  mutation createIssueMutation($input: CreateIssueInput!) {
    createIssue(input: $input) {
      clientMutationId
      issue{
        id
      }
    }
  }
`;

export const createIssueMutation = ({
  title, description, priority,repositoryId, ...rest
}) => commitMutation(
    env,
    {
      mutation,
      variables: { input: {  title, description, repositoryId, priority }},
      onCompleted: rest.onCompleted || (() => null),
      onError: rest.onError || (err => console.error(err)),
    }
);
