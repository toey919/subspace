import React from 'react';
import PropTypes from 'prop-types';
import MainGrid from 'components/shared/MainGrid';
import R from 'ramda';
import {compose, withProps} from 'recompose';
import { Button, Label, Grid, Row, Col, Clearfix} from 'react-bootstrap'
import {ContainerSty, HeaderSty, FooterSty, LabelSty, MenuButton} from './styles.js';
import Editor from 'react-quill';
import {toJS} from 'immutable';
import { connect } from 'react-redux';
import { injectSelectors } from 'redux/utils'
import { Field, reduxForm, SubmissionError } from 'redux-form/immutable';
import { TextInput } from 'components/shared/form'
import Separator from 'components/shared/Separator';
import { createIssueMutation } from 'relay';
import {withRouter} from 'react-router-dom'
import { matchRoute } from 'utils/routeMatcher';


const validators = {
	required : value => (value ? undefined : 'Required'),
	minValue : min => value => value && value < min ? `Must be at least ${min}` : undefined,
	maxValue : max => value => value && value < max ? `Cannot be more than ${max}` : undefined
}

const CreateIssueForm = ({handleEditorChange, handleSubmit, change, createIssue }) => (
	<MainGrid>
	 <form onSubmit={handleSubmit}>
	 	<ContainerSty>
	 		<HeaderSty>
		 		<h3>New Issue</h3>
	 		</HeaderSty>
	 		<Separator/>
	 		<Field
	 		  name="title"
	 		  placeholder="Please Provide a title for the issue"
	 		  component={TextInput}
	 		  type="text"
	 		  width="100%"
	 		/>
	 		<Field
	 			name="description"
	 			component = {Editor}
	 			{...handleEditorChange}
	 		/>
	 	</ContainerSty>
	 	<FooterSty>
	 		<LabelSty>Set a priority for this issue (0-9)</LabelSty>
	 		<Field
	 			name="priority"
	 			component={TextInput}
	 			width="60%"
	 			placeholder="Please Set a priority value"
	 			type="number"
	 		/>
	 	</FooterSty>
	 	<Separator/>
	 	<MenuButton bsStyle="success" type="submit">Submit</MenuButton>
	 </form>
	</MainGrid>
);

CreateIssueForm.propTypes = {
	handleSubmit: PropTypes.func.isRequired,
	change: PropTypes.func.isRequired,
	createIssue: PropTypes.object.isRequired,
	handleEditorChange: PropTypes.object.isRequired
}

const maybeImmutableObject = (state) => {
	return state!=null && state.toObject() || {priority:0};
}

export default compose(
	withRouter,
  reduxForm({
    form: 'createIssue',
    enableReinitialize: true,
    onSubmit: async (values, _, {vars:{userName,projectName},history, repository:{rawId}}) => {
      const { title, description, priority } = values.toObject();
      if([title, description, priority].filter(v => validators.required(v)).length==0){
      	let repositoryId = rawId;
      	let numPriority = Number(priority);
      	createIssueMutation({
					title, description, priority:numPriority, repositoryId,
					onCompleted: resp => {
						console.log("Created Issue, TODO: Add modal/some sort of information");
    				history.push(`/${userName}/${projectName}/issues`);
          },
          onError: (err) => {
          	console.log(`TODO: Handle ${err}`);
          }
      	});
      }
    },
  }),
  withProps(
		({change}) => ({
			handleEditorChange:{
				props: {
					onChange: (content) => {
						change("description",content);
					}
				}
			}
		})
	),
  connect(state => ({
    createIssue: maybeImmutableObject(state.getIn(['form','createIssue','values']))
    ,
  }))
)(CreateIssueForm);
