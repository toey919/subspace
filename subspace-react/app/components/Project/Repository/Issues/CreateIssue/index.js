import React from 'react';
import PropTypes from 'prop-types';
import RepositoryQueryRenderer from 'relay/RepositoryQueryRenderer';
import ComposeCreateIssue from './createIssueForm';


const CreateIssue = ({ vars }) => (
  <RepositoryQueryRenderer vars={vars} query={query}>
    <ComposeCreateIssue vars={vars} />
  </RepositoryQueryRenderer>
)

CreateIssue.propTypes = {
  vars: PropTypes.object.isRequired,
};

const query = graphql`
  query CreateIssueRepositoryQuery(
    $userName: String!, $projectName: String!,
  ) {
    viewer {
      repository(ownerName: $userName, name: $projectName) {
      	rawId
      }
    }
  }
`;

export default CreateIssue;