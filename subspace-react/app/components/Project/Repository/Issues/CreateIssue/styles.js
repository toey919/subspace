import styled from 'styled-components';
import {Button, Label} from 'react-bootstrap';


export const ContainerSty = styled.div`
	padding: 0px;
`;

export const HeaderSty = styled.div`
	padding: 0px;
	&:after{
		content:"";
		clear:both;
		display:block;
	}
`;

export const FooterSty = styled.div`
	margin-top:5px;
	.btn{
		float:right;
	}
`;

export const LabelSty = styled.p`
	font-size:16px;
	margin-bottom:0px;
	font-weight:bold;
	color:#666;
`
export const MenuButton = styled(Button)`
	float:right;
`;