import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-relay';
import withRelayFragment from 'relay/withRelayFragment';
import RepositoryQueryRenderer from 'relay/RepositoryQueryRenderer';
import { compose, mapProps } from 'recompose';
import { withRouter } from 'react-router-dom';
import MainGrid from 'components/shared/MainGrid';
import { matchRoute } from 'utils/routeMatcher';
import { PanelSty, UlSty, TableSty, colPriority} from './styles';
import Issue from './Issue';
import Menu from './Menu';

const Issues = ({
  userName, projectName, edges, totalIssuesClosed, totalIssuesOpen
}) => (
  <MainGrid>
    <Menu 
      totalIssuesClosed={totalIssuesClosed} 
      totalIssuesOpen={totalIssuesOpen}
      userName = {userName}
      projectName = {projectName}
    /> 
    <UlSty>
      {edges.map(edge => edge.node).map(node =>
          <Issue key={node.id} issueItem={node} />
      )}
    </UlSty>
  </MainGrid>
)

const ComposeIssues = compose(
  mapProps(({
    vars: {userName, projectName},
    repository: { issues: { edges, totalIssuesClosed, totalIssuesOpen }},
  }) => ({userName, projectName, edges, totalIssuesClosed, totalIssuesOpen }))
)(Issues)

const IssuesQuery = ({ vars }) => (
  <RepositoryQueryRenderer vars={vars} query={query}>
    <ComposeIssues vars={vars} />
  </RepositoryQueryRenderer>
)

IssuesQuery.propTypes = {
  vars: PropTypes.object.isRequired,
};

const query = graphql`
  query IssuesQuery(
    $userName: String!, $projectName: String!,
  ) {
    viewer {
      repository(ownerName: $userName, name: $projectName) {
        issues{
          totalIssuesOpen
          totalIssuesClosed
          edges {
            node {
              id
              rawId 
              title 
              createdAt 
              isClosed 
              priority 
              createdBy 
            }
          }
        }
      }
    }
  }
`;

export default IssuesQuery;
