import styled from 'styled-components';
import {Panel, Table} from 'react-bootstrap';

export const PanelSty = styled(Panel)`
  border-radius:0px;
`
export const TableSty = styled(Table)`
  border: 1px solid #ddd;
  margin-top: 10px;
  table-layout:fixed;
`
export const UlSty = styled.ul`
	list-style-type:none;
	padding:0;
	margin:0;
`


