import styled from 'styled-components';
import {Button} from 'react-bootstrap';

export const Container = styled.div`
  border-radius:0px;
  margin: 10px 0px;
  padding: 15px 0px;
`;

export const ButtonRight = styled(Button)`
	float:right;
`;

export const InfoButtonSty = styled(Button)`
	span{
		margin-left: 10px;
	}
`
