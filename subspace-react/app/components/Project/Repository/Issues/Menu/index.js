import React from 'react';
import PropTypes from 'prop-types';
import {Container,InfoButtonSty, ButtonRight} from './styles';
import {ButtonGroup, Button, Badge} from 'react-bootstrap';
import { graphql } from 'react-relay';
import withRelayFragment from 'relay/withRelayFragment';
import { compose, mapProps } from 'recompose';
import { Link } from 'react-router-dom'

const InfoButton = ({label, data}) =>(
  <InfoButtonSty>{label}<Badge>{data}</Badge></InfoButtonSty>
)

const Menu = ({
  totalIssuesOpen = 0 ,
  totalIssuesClosed = 0,
  userName,
  projectName
}) => 
(
  <Container>
    <ButtonGroup>
      <InfoButton label="Open" data={totalIssuesOpen}></InfoButton>
      <InfoButton label="Closed" data={totalIssuesClosed}></InfoButton>
    </ButtonGroup>
    <Link to={`/${userName}/${projectName}/issues/new`}>
      <ButtonRight bsStyle="success">Create New Issue</ButtonRight>
    </Link>
  </Container>
);


Menu.propTypes = {
  totalIssuesOpen: PropTypes.number,
  totalIssuesClosed: PropTypes.number,
  userName: PropTypes.string.isRequired,
  projectName: PropTypes.string.isRequired
};
export default Menu;


