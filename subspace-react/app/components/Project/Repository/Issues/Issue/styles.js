import styled from 'styled-components';
import {Panel, Badge, Col} from 'react-bootstrap';

export const PanelSty = styled(Panel)`
  border-radius:0px;
  margin-bottom: 5px;
  box-shadow: 0 2px 4px rgba(0,0,0,0.08), 0 2px 4px rgba(0,0,0,0.11);
  div.panel-body{
		padding: 10px 15px;
  }
`;
const pTruncated = styled.p`
	max-width:100%;
	text-overflow: ellipsis;
	white-space: nowrap; 
  overflow: hidden;
`
export const Title = styled(pTruncated)`
	font-size: 16px;
  margin-bottom:4px;
  a{
  	color:#000;
  	&:hover{
  		color: #0366d6;
  	}
  }
`;
export const Sub = styled(pTruncated)`
	color: #666;
	font-size: 14px;
`;
export const InfoBadgeSty = styled.div`
	color: #222;
	font-size:16px;
`;
export const BadgeSty = styled(Badge)`
	padding: 3px 6px;
	font-size:12px;
	color: #fff;
	background: #0366d6;
	margin-left:5px;
`;
