import React from 'react';
import {LinkIssue} from 'components/shared/Links';
import PropTypes from 'prop-types';
import {PanelSty, Title, Sub} from './styles';
import {Col, Grid, Row, Badge} from 'react-bootstrap';
import {IssueStat} from './Components/index';
import {calcDaysDiff} from 'utils/dateUtils';

const generateSubMessage = (createdAt, createdBy) => {
  let dateDiff = calcDaysDiff(createdAt);
  if(dateDiff>1)
    return `Opened ${dateDiff} days ago by ${createdBy}`;
  return `Opened a day ago by ${createdBy}`;
}
const IssueInfo = ({id, title, createdAt, isClosed, createdBy}) => (
  <Col sm={6} md={8}>
      <Title>
        <LinkIssue to={id}>{title}</LinkIssue>
      </Title>
      <Sub>{generateSubMessage(createdAt,createdBy)}</Sub>
  </Col>
);

const PriorityInfo = ({priority=0}) => (
  <Col sm={6} md={4}>
    <IssueStat label="DOWNVOTES" data={0} color="#c0392b"/>
    <IssueStat label="UPVOTES" data={0} color="#16a085"/>
    <IssueStat label="PRIORITY" data={priority}/>
  </Col>
);


const Issue = ({
  issueItem,
  issueItem : {
    priority
  }
}) => 
(
  <PanelSty>
    <IssueInfo {...issueItem}></IssueInfo>
    <PriorityInfo priority={priority}></PriorityInfo>
  </PanelSty>
);

Issue.propTypes = {
  issueItem: PropTypes.object.isRequired
};

export default Issue;

