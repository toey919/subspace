import styled from 'styled-components';
export const IssueStatSty = styled.div`
	margin:0px 10px;
	display:inline-block;
	float:right;
	transition: .1s transform linear;
	&:hover{
		transform: scale(1.1)
	}
`;

export const BaseSty = styled.div`
	text-align:center;
	color:inherit;
`;

export const ContentSty = styled(BaseSty)`
	margin-top:2px;
	font-size: 22px;
`;

export const LabelSty = styled(BaseSty)`
	font-size: 12px;
	text-transform:uppercase;
`;