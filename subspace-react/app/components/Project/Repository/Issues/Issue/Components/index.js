import React from 'react';
import {IssueStatSty, ContentSty, LabelSty} from './styles';
export const IssueStat = ({label, data, color="#222"}) => (
  <IssueStatSty style={{color:color}}>
    <ContentSty>{data}</ContentSty>
    <LabelSty>{label}</LabelSty>
  </IssueStatSty>
);