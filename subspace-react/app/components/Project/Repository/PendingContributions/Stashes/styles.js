import styled from 'styled-components';
import MainGridBase from 'components/shared/MainGrid';
import PaperBase from 'material-ui/Paper';

const flexStr = `
  display: flex;
  flex-direction: column;
  flex: 1;
`

export const VoteContainer = styled.div`
  margin-top: 10px;
  margin-bottom: 20px;
`

export const MainGrid = styled(MainGridBase)`
  ${flexStr}
`

export const FlexDiv = styled.div`
  ${flexStr}
`

export const Paper = styled(PaperBase)`
  margin-top: 20px;
  width: 300px;
  padding: 10px;
  text-align: center;
  background: #FFE0B2;
`

export const NotifDiv = styled.div`
  ${flexStr}
  align-items: center;
`
