import styled from 'styled-components';
import DividerBase from 'material-ui/Divider';
import Card, { CardHeader, CardContent } from 'material-ui/Card';

export const PendingMainGrid = styled.div`
  font-weight: 400;
  padding-top: 20px;
`

export const Divider = styled(DividerBase)`
  margin-bottom: 12px !important;
`

export const MainCard = styled(Card)`
  box-shadow: none !important;
  font-family: "Roboto", sans-serif !important;
  & > div > .expandIcon {
    color: rgba(255,255,255,0.8) !important;
  }
`

export const MainCardHeader = styled(CardHeader)`
  background-color: ${props => props['data-color']} !important;
`

export const MainCardContent = styled(CardContent)`
  margin-top: 10px;
  padding: 0px !important;
`

export const TitleDiv = styled.div`
  color: rgba(255,255,255,1);
  font-size: 16px;
`

export const SubheaderDiv = styled.div`
  color: rgba(255,255,255,0.7);
`

export const DescriptionCardContent = styled(CardContent)`
  padding: 16px 20px !important;
`
