import styled from 'styled-components';
import CardBase from 'material-ui/Card';
import DividerBase from 'material-ui/Divider';
import AvatarBase from 'material-ui/Avatar';
import { TableRow } from 'material-ui/Table';

export const SpanAccept = styled.span`
  color: #4CAF50;
`

export const SpanReject = styled.span`
  color: #F44336;
`

export const SpanPoint = styled.span`
  margin-right: 20px;
`

export const Divider = styled(DividerBase)`
`

export const Card = styled(CardBase)`
  border: 0px !important;
  box-shadow: none !important;
  & div {
    padding: 0px !important;
  }
  & .cardContent {
    padding-top: 15px !important;
    padding-bottom: 0px !important;
  }
`

export const Avatar = styled(AvatarBase)`
  width: 64px !important;
  height: 64px !important;
`

export const HeadTableRow = styled(TableRow)`
  height: 48px !important;
`
