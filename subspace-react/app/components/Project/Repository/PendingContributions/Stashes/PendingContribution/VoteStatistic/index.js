import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-relay';
import withRelayFragment from 'relay/withRelayFragment';
import { compose, mapProps } from 'recompose';
import { CardHeader, CardContent } from 'material-ui/Card';
import Table, { TableBody, TableCell, TableHead, TableRow } from 'material-ui/Table';
import pluralize from 'pluralize';
import CircularProgressbar from 'react-circular-progressbar';
import UserVoteList from './UserVoteList';
import {
  SpanAccept, SpanReject, SpanPoint, Card, Avatar, Divider, HeadTableRow,
} from './styles';

const VoteStatistic = ({
  stash: { acceptVotes, rejectVotes, voteTreshold },
  totalCount, votePercentage, totalVotePoints,
}) => (
  <Card>
    <CardHeader
      title="Vote Statistic"
      subheader={(
        <span>
          <div>
            There{' '}
            {pluralize('is', totalCount)}{' '}
            {pluralize('voter', totalCount, true)}{' with total of '}
            <SpanAccept>
              {acceptVotes.totalVotePoints}
            </SpanAccept>{' '}
            {pluralize(
              'acceptance point',
              acceptVotes.totalVotePoints
            )}{' and '}
            <SpanReject>
              {rejectVotes.totalVotePoints}
            </SpanReject>{' '}
            {pluralize(
              'rejection point',
              rejectVotes.totalVotePoints
            )}
          </div>
          <div>
            <SpanPoint>
              {pluralize(
                'Current point',
                totalVotePoints
              )}: {totalVotePoints}
            </SpanPoint>
            <SpanPoint>
              {pluralize(
                'Treshold point',
                voteTreshold
              )}: {voteTreshold}
            </SpanPoint>
            <SpanPoint>
              {pluralize(
                'Point',
                voteTreshold - totalVotePoints
              )} needed: {voteTreshold - totalVotePoints}
            </SpanPoint>
          </div>
        </span>
      )}
      avatar={
        <Avatar style={{ backgroundColor: 'inherit' }}>
          <CircularProgressbar
            strokeWidth={12}
            percentage={votePercentage}
          />
        </Avatar>
      }
    />
    <CardContent className="cardContent">
      <Divider />
      <Table>
        <TableHead>
          <HeadTableRow>
            <TableCell>Accepted by</TableCell>
            <TableCell>Rejected by</TableCell>
            <TableCell>Revise Suggested by</TableCell>
          </HeadTableRow>
        </TableHead>
        <TableBody>
          <TableRow>
            <TableCell>
              <UserVoteList
                stashVoteConnection={acceptVotes}
              />
            </TableCell>
            <TableCell>
              <UserVoteList
                stashVoteConnection={rejectVotes}
              />
            </TableCell>
            <TableCell>
            </TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </CardContent>
  </Card>
)

VoteStatistic.propTypes = {
  stash: PropTypes.object.isRequired,
  totalCount: PropTypes.number.isRequired,
  votePercentage: PropTypes.number.isRequired,
  totalVotePoints: PropTypes.number.isRequired,
};

export default compose(
  withRelayFragment({
    stash: graphql`
      fragment VoteStatistic_stash on Stash {
        voteTreshold
        votes {
          totalCount
          totalVotePoints
        }
        acceptVotes {
          totalVotePoints
          ...UserVoteList_stashVoteConnection
        }
        rejectVotes {
          totalVotePoints
          ...UserVoteList_stashVoteConnection
        }
      }
    `,
  }),
  mapProps(props => {
    const {
      stash: {
        voteTreshold,
        votes,
      },
    } = props

    const totalVotePoints = votes ? votes.totalVotePoints : 0;

    return {
      totalCount: votes.totalCount || 0,
      votePercentage: totalVotePoints <= 0 ? 0 :
        Math.round((totalVotePoints / voteTreshold) * 100),
      totalVotePoints,
      ...props,
    }
  }),
)(VoteStatistic)
