import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-relay';
import withRelayFragment from 'relay/withRelayFragment';
import { compose, mapProps } from 'recompose';
import Card from 'material-ui/Card';
import Avatar from 'material-ui/Avatar';
import moment from 'moment';
import UserPhoto from 'components/shared/UserPhoto';
import Commits from './Commits';
import VoteStatistic from './VoteStatistic';
import {
  PendingMainGrid, Divider, MainCard, MainCardHeader, TitleDiv,
  SubheaderDiv, MainCardContent, DescriptionCardContent,
} from './styles';

const PendingContribution = ({
  title, pendingRef,
  cardColor,
  pendingRef: {
    stash: { description, owner, createdAt },
  },
}) => (
  <PendingMainGrid>
    <MainCard>
      <MainCardHeader
        title={<TitleDiv>{title}</TitleDiv>}
        data-color={cardColor}
        subheader={
          <SubheaderDiv>
            {`pushed ${moment(createdAt).fromNow()}`}
          </SubheaderDiv>
        }
        avatar={(
          <Avatar>
            <UserPhoto
              userName={owner.userName}
              photoUrl={owner.photoUrl}
              width={32}
              height={32}
            />
          </Avatar>
        )}
      />
      <MainCardContent>
        <Card>
          <DescriptionCardContent>
            {description && (
              <dl>
                <dt>Description</dt>
                <dd>
                  {description}
                </dd>
              </dl>
            )}
            <dl>
              <dt>Goals &amp; Issues</dt>
              <dd>TODO</dd>
            </dl>
            <dl>
              <dt>Contributor Statistic</dt>
              <dd>TODO</dd>
            </dl>
            <Divider />
            <VoteStatistic stash={pendingRef.stash} />
          </DescriptionCardContent>
        </Card>
        <Commits commit={pendingRef.target} />
      </MainCardContent>
    </MainCard>
  </PendingMainGrid>
)

PendingContribution.propTypes = {
  pendingRef: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  cardColor: PropTypes.string.isRequired,
}

export default compose(
  withRelayFragment({
    pendingRef: graphql`
      fragment PendingContribution_pendingRef on Ref {
        id
        stash {
          id
          stashNum
          title
          description
          createdAt
          owner {
            userName
            photoUrl
          }
          userVoteType
          ...VoteStatistic_stash
        }
        target {
          ... on Commit {
            ...Commits_commit
          }
        }
      }
    `,
  }),
  mapProps(({
    pendingRef: { stash: { stashNum, title, userVoteType } },
    pendingRef, ...rest
  }) => {
    let cardColor = '#039BE5'
    if (userVoteType !== null) {
      if (userVoteType === 'up') {
        cardColor = '#43A047'
      } else {
        cardColor = '#EF5350'
      }
    }
    return {
      title: title ?
        `${title} (Stash #${stashNum})` : `Stash #${stashNum}`,
      pendingRef,
      cardColor,
      ...rest,
    }
  })
)(PendingContribution)
