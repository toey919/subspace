import React from 'react';
import PropTypes from 'prop-types';
import { createFragmentContainer, graphql } from 'react-relay';
import { SpanUser, UserPhoto, UserName, SpanPoint } from './styles';

const UserVoteList = ({ stashVoteConnection, title }) => (
  <dl>
    {title &&
      <dt>
        {title}
      </dt>
    }
    <dd style={{ marginTop: 15 }}>
      {
      stashVoteConnection.totalCount < 1 ?
        'None yet' :
        stashVoteConnection.edges.map(({ node }) => (
          <SpanUser key={node.id}>
            <UserPhoto
              width={20}
              height={20}
              user={node.owner}
            />
            <UserName userName={node.owner.userName} />
            <SpanPoint
              data-votePoint={node.voteType === 'up' ? node.votePoint : -node.votePoint}
            >
              {node.voteType === 'up' ? node.votePoint : -node.votePoint}
            </SpanPoint>
          </SpanUser>
        ))
      }
    </dd>
  </dl>
)

UserVoteList.propTypes = {
  title: PropTypes.string,
  stashVoteConnection: PropTypes.object.isRequired,
};

export default createFragmentContainer(UserVoteList, {
  stashVoteConnection: graphql`
    fragment UserVoteList_stashVoteConnection on StashVoteConnection {
      totalCount
      edges {
        node {
          id
          votePoint
          voteType
          owner {
            userName
            photoUrl
          }
        }
      }
    }
  `,
})
