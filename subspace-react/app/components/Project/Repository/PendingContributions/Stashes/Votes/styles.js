import styled from 'styled-components';
import DividerBase from 'material-ui/Divider';
import BottomNavigationBase, {
  BottomNavigationButton as BottomNavigationButtonBase,
} from 'material-ui/BottomNavigation';
import { CardContent as CardContentBase } from 'material-ui/Card';

export const SpanAccept = styled.span`
  color: #4CAF50;
`

export const SpanReject = styled.span`
  color: #F44336;
`

export const Divider = styled(DividerBase)`
  margin-top: 10px !important;
  margin-bottom: 10px !important;
`

export const BottomNavigation = styled(BottomNavigationBase)`
  background: #F5F5F5 !important;
`

export const BottomNavigationButton = styled(BottomNavigationButtonBase)`
  padding: 0px !important;
`

export const CardContent = styled(CardContentBase)`
  padding: 10px !important;
  background: #F5F5F5 !important;
`
