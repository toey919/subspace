import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-relay';
import withRelayFragment from 'relay/withRelayFragment';
import { voteStashMutation, mergeStashMutation } from 'relay';
import { compose, withState, mapProps, withHandlers } from 'recompose';
import { withRouter } from 'react-router';
import { matchRoute } from 'utils/routeMatcher';
import { redirect } from 'redux/utils';
import { getProjectPath } from 'utils/path';
import pluralize from 'pluralize';
import MdCheck from 'react-icons/lib/md/check';
import MdClear from 'react-icons/lib/md/clear';
import MdEdit from 'react-icons/lib/md/edit';
import Card from 'material-ui/Card';
import {
  BottomNavigation, BottomNavigationButton, CardContent,
} from './styles';

pluralize.addIrregularRule('is', 'are')

const Votes = ({ voteIndex, onVote }) => (
  <Card>
    <CardContent>
      <BottomNavigation value={voteIndex}>
        <BottomNavigationButton
          showLabel
          label="Accept"
          icon={<MdCheck width={32} height={32} />}
          onClick={() => onVote('up')}
        />
        <BottomNavigationButton
          showLabel
          label="Reject"
          icon={<MdClear width={32} height={32} />}
          onClick={() => onVote('down')}
        />
        <BottomNavigationButton
          showLabel
          label="Revise"
          icon={<MdEdit width={32} height={32} />}
          onClick={() => onVote('revise')}
        />
      </BottomNavigation>
    </CardContent>
  </Card>
)

Votes.propTypes = {
  voteIndex: PropTypes.number,
  onVote: PropTypes.func.isRequired,
}

export default compose(
  withRelayFragment({
    stash: graphql`
      fragment Votes_stash on Stash {
        rawId
        voteTreshold
        votes {
          totalCount
          totalVotePoints
        }
        userVoteType
        acceptVotes {
          totalVotePoints
        }
        rejectVotes {
          totalVotePoints
        }
      }
    `,
  }),
  withRouter,
  withState(
    'voteType', 'updateVoteType',
    props => props.stash.userVoteType
  ),
  withState('isMerging', 'updateIsMerging', false),
  mapProps(props => {
    const {
      location: { pathname },
      voteType,
    } = props

    let voteIndex;
    switch (voteType) {
      case 'up':
        voteIndex = 0;
        break;
      case 'down':
        voteIndex = 1;
        break;
      case 'revise':
        voteIndex = 2;
        break;
      default:
        voteIndex = 99;
    }

    return {
      variables: matchRoute(pathname).params,
      voteIndex,
      ...props,
    }
  }),
  withHandlers({
    onVote: props => voteType => {
      props.onHandleVote(false)
      window.scrollTo(0, 0)
      const prevVoteType = props.voteType

      // Toggle vote
      if (props.voteType === voteType) {
        props.updateVoteType(null);
      } else {
        props.updateVoteType(voteType)
      }

      voteStashMutation({
        voteType,
        stashId: props.stash.rawId || null,
        onCompleted: resp => {
          setTimeout(() => {
            props.onHandleVote(true)
            props.onShowPoint(false)
            if (resp.voteStash.clientMutationId === null) {
              props.updateVoteType(prevVoteType);

              // TODO: add login first flow
              alert('Login first');
              return;
            }

            const { voteStash: { stash } } = resp;
            const acceptVotePoints = stash.acceptVotes ? stash.acceptVotes.totalVotePoints : 0;
            const rejectVotePoints = stash.rejectVotes ? stash.rejectVotes.totalVotePoints : 0;
            const totalPoints = acceptVotePoints + rejectVotePoints;
            if (totalPoints >= stash.voteTreshold) {
              props.updateIsMerging(true);
              mergeStashMutation({
                id: props.repository.id,
                stashName: props.name,
                repositoryId: props.repository.rawId,
                onCompleted: () => {
                  props.updateIsMerging(false);
                  redirect(`${getProjectPath(props.variables)}`);
                },
              })
            }
          }, 700)
          props.onShowPoint(true)
        },
      });
    },
  })
)(Votes)
