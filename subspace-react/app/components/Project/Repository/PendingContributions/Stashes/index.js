import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-relay';
import withRelayFragment from 'relay/withRelayFragment';
import { compose, withState } from 'recompose';
import FadeTransition from 'components/shared/FadeTransition';
import Typography from 'material-ui/Typography';
import Divider from 'material-ui/Divider';
import PendingContribution from './PendingContribution';
import ReviewLeftStat from './ReviewLeftStat';
import Votes from './Votes';
import Comments from './Comments';
import { VoteContainer, MainGrid, FlexDiv, Paper, NotifDiv } from './styles';

const Stashes = ({
  repository: { totalStashes: { totalCount }, stashes, stashes: { edges } },
  inProp, setInProp, isShowPoint, setIsShowPoint,
}) => (
  <MainGrid>
    {
      edges && edges.length !== 0 &&
      <div>
        <ReviewLeftStat totalContribution={totalCount} refConnection={stashes} />
      </div>
    }
    <FadeTransition in={isShowPoint}>
      <NotifDiv>
        <Paper
          elevation={3}
          style={{
            position: 'fixed',
            textAlign: 'center',
          }}
        >
          <Typography type="body1" component="p">
            Review Submitted
          </Typography>
        </Paper>
      </NotifDiv>
    </FadeTransition>
    <FlexDiv>
      {
        edges && edges.map(({ node, node: { id } }) =>
          (<FlexDiv key={id}>
            <FadeTransition in={inProp}>
              <PendingContribution pendingRef={node} />
              <VoteContainer>
                <Votes
                  stash={node.stash}
                  onHandleVote={setInProp}
                  onShowPoint={setIsShowPoint}
                />
              </VoteContainer>
              <Divider />
              <Comments stash={node.stash} />
            </FadeTransition>
          </FlexDiv>)
        )
      }
      <FadeTransition in={!isShowPoint}>
        {
          edges && edges.length === 0 && !isShowPoint &&
          <h4 style={{ marginTop: 30 }}>
            No pending contributions left to review
          </h4>
        }
      </FadeTransition>
    </FlexDiv>
  </MainGrid>
)

Stashes.propTypes = {
  repository: PropTypes.object,
  inProp: PropTypes.bool.isRequired,
  setInProp: PropTypes.func.isRequired,
  isShowPoint: PropTypes.bool.isRequired,
  setIsShowPoint: PropTypes.func.isRequired,
}

export default compose(
  withRelayFragment({
    repository: graphql`
      fragment Stashes_repository on Repository {
        totalStashes: stashes {
          totalCount
        }
        stashes(first: 1, isVoted: false){
          ...ReviewLeftStat_refConnection
          edges {
            node {
              id
              ...PendingContribution_pendingRef
              stash {
                ...Votes_stash
                ...Comments_stash
              }
            }
          }
        }
      }
    `,
  }),
  withState('inProp', 'setInProp', true),
  withState('isShowPoint', 'setIsShowPoint', false),
)(Stashes)
