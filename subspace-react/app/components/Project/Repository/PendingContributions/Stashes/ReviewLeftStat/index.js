import React from 'react';
import { graphql } from 'react-relay';
import withRelayFragment from 'relay/withRelayFragment';
import { compose } from 'recompose';

export default compose(
  withRelayFragment({
    refConnection: graphql`
      fragment ReviewLeftStat_refConnection on RefConnection {
        totalCount
      }
    `,
  })
)(
  ({ totalContribution, refConnection: { totalCount } }) => (
    <h3 style={{ textAlign: 'center' }}>
      Contribution left: {totalCount} / {totalContribution}
    </h3>
  )
)
