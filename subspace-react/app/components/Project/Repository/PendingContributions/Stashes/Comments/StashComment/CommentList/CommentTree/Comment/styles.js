import styled, { keyframes } from 'styled-components';
import { Panel } from 'react-bootstrap';

export const MainDiv = styled.div`
  position: relative;
  padding-left: 26px;
  flex: 1;
`

export const DivLinkPhoto = styled.div`
  float: left;
  margin-left: -38px;
  border-radius: 3px;
`

export const PaddingKeyframes = isShowContent => keyframes`
  from {
    padding-top: ${isShowContent ? '0' : '7'}px;
    padding-bottom: ${isShowContent ? '0' : '7'}px;
  }
  to {
    padding-top: ${isShowContent ? '7' : '0'}px;
    padding-bottom: ${isShowContent ? '7' : '0'}px;
  }
`

export const PanelComment = styled(Panel)`
  border: 0px;
  margin-bottom: 10px;
  box-shadow: none;
  & .panel-heading {
    padding: 5px 10px;
    background: #fcfcfc;
    color: rgba(0,0,0,0.4);
    border: 1px solid ${props => {
      let color = '#ddd';
      if (props['data-ownerVoteType'] !== null) {
        if (props['data-ownerVoteType'] === 'up') {
          color = 'rgba(45, 132, 48, 0.6)'
        }
        if (props['data-ownerVoteType'] === 'down') {
          color = 'rgba(169, 12, 12, 0.6)'
        }
      }
      return color
    }};
    border-left: ${props => {
      let border = '1px solid #ddd'
      if (props['data-ownerVoteType'] !== null) {
        if (props['data-ownerVoteType'] === 'up') {
          border = '6px solid rgba(45, 132, 48, 0.6)'
        }
        if (props['data-ownerVoteType'] === 'down') {
          border = '6px solid rgba(169, 12, 12, 0.6)'
        }
      }
      return border
    }};
  }
  & .panel-body {
    color: rgba(0,0,0,0.9);
    -webkit-animation: ${props => PaddingKeyframes(props['data-isShowContent'])} 0.3s; /* Safari 4+ */
    -moz-animation:    ${props => PaddingKeyframes(props['data-isShowContent'])} 0.3s; /* Fx 5+ */
    -o-animation:      ${props => PaddingKeyframes(props['data-isShowContent'])} 0.3s; /* Opera 12+ */
    animation:         ${props => PaddingKeyframes(props['data-isShowContent'])} 0.3s; /* IE 10+, Fx 29+ */
    border-left: 1px solid #ddd;
    border-right: 1px solid #ddd;
    padding: 0px;
    padding-top: ${props => props['data-isShowContent'] ? '7' : '0'}px;
    padding-bottom: ${props => props['data-isShowContent'] ? '7' : '0'}px;
    padding-right: 15px;
    padding-left: 15px;
    ${props => props['data-isShowContent'] ? '' : 'border: 0px;'}
    & p {
      margin: 0px;
    }
  }
  & .panel-footer {
    border: 1px solid #ddd;
    padding: 5px 10px;
  }
`
