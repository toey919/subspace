import styled from 'styled-components';
import CardBase, {
  CardHeader as CardHeaderBase,
  CardContent as CardContentBase,
} from 'material-ui/Card';

export const Card = styled(CardBase)`
  box-shadow: none !important;
`;

export const CardHeader = styled(CardHeaderBase)`
  padding: 0px !important;
  margin-bottom: 5px !important;
`

export const CardContent = styled(CardContentBase)`
  padding: 0px !important;
`
