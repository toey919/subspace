import React from 'react';
import PropTypes from 'prop-types';
import { graphql } from 'react-relay';
import RepositoryQueryRenderer from 'relay/RepositoryQueryRenderer';
import Stashes from './Stashes'

const PendingContributions = ({ vars }) => (
  <RepositoryQueryRenderer vars={vars} query={query}>
    <Stashes />
  </RepositoryQueryRenderer>
)

PendingContributions.propTypes = {
  vars: PropTypes.object.isRequired,
};

const query = graphql`
  query PendingContributionsQuery(
    $userName: String!, $projectName: String!, $sort: String!
  ) {
    viewer {
      repository(ownerName: $userName, name: $projectName) {
        ...Stashes_repository
      }
    }
  }
`;

export default PendingContributions;
