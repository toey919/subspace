import React from 'react';
import PropTypes from 'prop-types';
import { compose, withState, withHandlers, mapProps } from 'recompose';
import { graphql } from 'react-relay';
import withRelayFragment from 'relay/withRelayFragment';
import { Grid } from 'react-bootstrap';
import RepoLink from 'components/shared/repo/TitleLink'
import Particles from 'react-particles-js';
import GoCode from 'react-icons/lib/go/code'
import GoIssueOpened from 'react-icons/lib/go/issue-opened'
import GoRepoPush from 'react-icons/lib/go/repo-push';
import FaObjectGroup from 'react-icons/lib/fa/object-group';
import MdHome from 'react-icons/lib/md/home';
import { withRouter } from 'react-router-dom';
import R from 'ramda';
import Granim from 'granim';
import { normalize, schema } from 'normalizr';
import particles from './particles'
import {
  RepoTitle, NavLabel, Icon, MainNavTabs, NavItem, Badge,
} from './styles';

const getNavConfig = ({ owner: { userName }, name, stashes: { totalCount } }) => [
  {
    link: `/${userName}/${name}/#home`,
    label: (
      <NavLabel>
        <Icon><MdHome /></Icon> Home
      </NavLabel>
    ),
    gradients: [['#fefefe', '#cccccc']],
  },
  {
    link: `/${userName}/${name}`,
    label: (
      <NavLabel>
        <Icon><GoCode /></Icon> Code
      </NavLabel>
    ),
    gradients: [['#fefefe', '#cccccc']],
  },
  {
    link: `/${userName}/${name}/issues`,
    label: (
      <NavLabel>
        <Icon><GoIssueOpened /></Icon> Goals &amp; Issues
      </NavLabel>
    ),
    gradients: [['#333b43', '#626b74']],
    isWhiteFont: true,
  },
  {
    link: `/${userName}/${name}/metaspace`,
    label: (
      <NavLabel>
        <Icon><FaObjectGroup /></Icon> Metaspace
      </NavLabel>
    ),
    gradients: [['#333b43', '#626b74']],
    isWhiteFont: true,
  },
  {
    link: `/${userName}/${name}/master/pendingcontributions`,
    label: (
      <NavLabel>
        <Icon><GoRepoPush /></Icon> Pending Contributions
        <Badge badgeContent={totalCount} color="accent">
          <span />
        </Badge>
      </NavLabel>
    ),
    gradients: [['#3561bf', '#00a8cb']],
    isWhiteFont: true,
  },
]

class TopContainer extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    if (this.granimCanvas) {
      const {
        activeKey,
        navs: { entities: { navs }, result },
        repository: { name, owner },
      } = this.props

      const gradientStates = R.mergeAll(result.map(nav => ({
        [nav]: {
          gradients: navs[nav].gradients,
          loop: false,
        },
      })))

      const currentActiveKey = navs[activeKey] ?
        activeKey : `/${owner.userName}/${name}`

      this.props.setGranimInstance(new Granim({
        element: this.granimCanvas,
        name: 'basic-gradient',
        direction: 'diagonal',
        opacity: [1, 1],
        isPausedWhenNotInView: true,
        stateTransitionSpeed: 500,
        states: {
          'default-state': {
            gradients: gradientStates[currentActiveKey].gradients,
            loop: false,
          },
          ...gradientStates,
        },
      }))
    }
  }

  render() {
    const {
      activeKey, handleSelect,
      navs: { entities: { navs }, result },
      repository: { name, owner, isPrivate },
    } = this.props
    return (
      <div style={{ borderTop: '2px solid #777' }}>
        <canvas
          ref={el => { this.granimCanvas = el }}
          style={{
            position: 'absolute',
            zIndex: 0,
            width: '100%',
            height: 110,
            borderBottom: '1px solid #ddd',
          }}
        ></canvas>
        <div style={{ position: 'absolute', zIndex: 0 }}>
          <Particles
            params={particles}
            height={120}
            width={'100%'}
          />
        </div>
        <Grid>
          <RepoTitle style={{ position: 'relative' }}>
            <RepoLink
              repoName={name}
              isPrivate={isPrivate}
              userName={owner.userName}
              isWhite={navs[activeKey] ? navs[activeKey].isWhiteFont : undefined}
              isHead
            />
          </RepoTitle>
          <MainNavTabs
            bsStyle="tabs"
            onSelect={handleSelect}
            activeKey={
              navs[activeKey] ?
                activeKey : `/${owner.userName}/${name}`
            }
          >
            {result.map(link => (
              <NavItem
                key={link}
                eventKey={link}
                data-isWhite={navs[activeKey] ? navs[activeKey].isWhiteFont : undefined}
              >
                {navs[link].label}
              </NavItem>
            ))}
          </MainNavTabs>
        </Grid>
      </div>
    )
  }
}

TopContainer.propTypes = {
  repository: PropTypes.object.isRequired,
  activeKey: PropTypes.string,
  navs: PropTypes.object.isRequired,
  handleSelect: PropTypes.func.isRequired,
  setGranimInstance: PropTypes.func.isRequired,
}


const navSchema = new schema.Entity('navs', {}, { idAttribute: 'link' })
const navsSchema = new schema.Array(navSchema);

export default compose(
  withRouter,
  withRelayFragment({
    repository: graphql`
      fragment TopContainer_repository on Repository {
        name
        isPrivate
        owner {
          userName
        }
        stashes {
          totalCount
        }
      }
    `,
  }),
  withState('activeKey', 'setActiveKey',
    props => props.history.location.pathname
  ),
  withState('granimInstance', 'setGranimInstance', null),
  withHandlers({
    handleSelect: props => link => {
      props.setActiveKey(link)
      props.history.push(link)
      if (props.granimInstance) {
        props.granimInstance.changeState(link)
      }
    },
  }),
  mapProps(props => {
    const { owner, name, stashes } = props.repository
    const arrayNavs = getNavConfig({ owner, name, stashes })
    const navs = normalize(arrayNavs, navsSchema);
    return {
      navs,
      ...props,
    }
  }),
)(TopContainer);
