import React from 'react';
import PropTypes from 'prop-types';
import Measure from 'react-measure';
import { compose, withState, withHandlers, mapProps } from 'recompose';
import Transition from 'react-transition-group/Transition';

const duration = 500;

const defaultStyle = {
  transition: `all ${duration}ms ease-in-out`,
  maxHeight: 10,
  width: '100%',
}

const HeightTransition = ({
  in: inProp, transitionStyles, children, handleOnResize,
}) => (
  <Measure
    onResize={handleOnResize}
  >
    {({ measureRef }) =>
      (<Transition in={inProp} timeout={duration}>
        {state => (
          <div
            style={{
              ...defaultStyle,
              ...transitionStyles[state],
            }}
          >
            <div ref={measureRef}>
              {children}
            </div>
          </div>
        )}
      </Transition>)
    }
  </Measure>
);

HeightTransition.propTypes = {
  in: PropTypes.bool.isRequired,
  children: PropTypes.node,
  transitionStyles: PropTypes.object.isRequired,
  handleOnResize: PropTypes.func.isRequired,
}

export default compose(
  withState('height', 'setHeight', 0),
  withHandlers({
    handleOnResize: props => val => {
      props.setHeight(val.entry.height)
    },
  }),
  mapProps(({ setHeight, ...rest }) => ({
    transitionStyles: {
      entering: { maxHeight: rest.height },
      entered: { maxHeight: rest.height },
    },
    ...rest,
  })),
)(HeightTransition);
