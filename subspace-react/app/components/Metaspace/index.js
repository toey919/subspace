import React from 'react';
import { App } from 'metaspace';
require('metaspace/dist/app.css');

const Metaspace = () => (
  <div className="container">
    <App />
  </div>
);

Metaspace.propTypes = {

};

export default Metaspace;
